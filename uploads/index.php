<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);

if(!is_dir("files")) mkdir("files", 0777, True);

require('UploadHandler.php');
$upload_handler = new UploadHandler();

require_once(dirname(dirname(__FILE__)) . "/system/class/files.class.php");
require_once(dirname(dirname(__FILE__)) . "/system/class/db.class.php");
require_once(dirname(dirname(__FILE__)) . "/system/class/logs.class.php");
$Files = new Files();

foreach($upload_handler->filesList as $file){
  $folder_upload= dirname(__FILE__)."/files";

  $file_name=$file->name;
  $file_location=$file->url; //tymczasowa lokalizacja pliku
  $plik_mime=$file->type; //typ MIME pliku wyslany przez przeglądarkę
  $file_size=$file->size;
  $file_error=$file->error; //kod blędu


  $allow_extensions=array("jpeg", "jpg", "tiff", "tif", "png", "gif", "doc", "pdf");
  $image_extensions=array("jpeg", "jpg", "tiff", "tif", "png", "gif");

  $file_extentions=pathinfo(strtolower($file_name), PATHINFO_EXTENSION);

   $date = date("Ymd");
   if(!is_dir($folder_upload."/".$file_extentions)){
	    mkdir($folder_upload."/".$file_extentions, 0777, True);
   }
   $file_newname = $date . "_" . md5($file_name) . "." . $file_extentions;
   $file_new = $folder_upload."/".$file_extentions."/".$file_newname;


   copy($file_location, $file_new) or Logs::add("Nie udalo się przenieść pliku. Lokalizacja:" . $file_new, $_COOKIE['uid']);;

  if (in_array($file_extentions, $image_extensions, true)) {
	   list($width_orig, $height_orig) = getimagesize($file_new);
	  $ratio_orig = $width_orig/$height_orig;
	  $width = array(200, 350);
	  $height = array(200, 350);
	  $widthC = $width;
	  $heightC = $height;
	  for($i = 0; $i<2; $i++){
		    if ($width[$i]/$height[$i] > $ratio_orig) {
   		     $width[$i] = $height[$i]*$ratio_orig;
		    } else {
   		     $height[$i] = $width[$i]/$ratio_orig;
		    }
		    $image_p = imagecreatetruecolor($width[$i], $height[$i]);
		    $image = imagecreatefromjpeg($file_new);
		    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width[$i], $height[$i], $width_orig, $height_orig);
		    $resizname = $date . "_" . md5($file_name) . "_".$widthC[$i]."x".$heightC[$i]."_." . $file_extentions;
		    $resize_file = $folder_upload."/".$file_extentions."/". $resizname;
		    if(!imagejpeg($image_p, $resize_file, 100))
          Logs::add("Nie udalo się przenieść pliku. Lokalizacja:" . $resize_file, $_COOKIE['uid']);
		    Logs::add("Zaladowano plik: ".$resizname, $_COOKIE['uid']);
		    $Files->add(array(
			  "name" => $resizname,
			  "location" => $file_extentions . "/",
			  "server_location" => $resize_file,
			  "mother_image" => $file_newname,
			  "size" => $widthC[$i]."x".$heightC[$i]
		    ));

	  }
	  Logs::add("Zaladowano plik: ".$file_newname, $_COOKIE['uid']);
	  $Files->add(array(
		    "name" => $file_newname,
		    "location" => $file_extentions . "/",
		    "server_location" => $file_new,
		    "mother_image" => $file_newname,
		    "size" => $width_orig."x".$height_orig
	  ));
  }else{
	   Logs::add("Zaladowano plik: ".$file_newname, $_COOKIE['uid']);
	  $Files->add(array(
		"name" => $file_newname,
		"location" => $file_extentions . "/",
		"server_location" => $file_new,
	  ));
  }
}
?>
