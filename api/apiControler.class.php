<?php
function obj2arr(stdClass $object) {
  $array = array();
  foreach ($object as $key => $value) {
    if ($value instanceof stdClass) {
      $array[$key] = obj2arr($value);
    } else {
      $array[$key] = $value;
    }
  }

  return $array;
}


class ApiControler extends AdminControler{

    private function getSetting($data){
      $result['status'] = "ERROR: parametr 'name' is not defined";
      if(isset($data->name) && $data->name){
        $value = $this->Settings->get($data->name);
        if($value){
          $result = array("status" => "OK", "value" => $value);
        }else{
          $result['status'] = "ERROR: Do not find ".$data['name']." parametr";
        }
      }
      return $result;
    }

    public function API($JSON){
      $data = json_decode($JSON);
      $result['status'] = "ERROR: Bad operation";
      if(isset($data->operation)){
        switch($data->operation){
          case 'getSetting':
            $result = $this->getSetting($data);
            break;
          default:
            $result['status'] = "ERROR: This operation is not implement";
            brak;
        }
      }
      return $result;
    }
}
?>
