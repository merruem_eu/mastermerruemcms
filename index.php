<?php
  /*********************************************************
  * Autor:                  Patryk Kurzeja, Patryk Hansz
  * Data utworzenia pliku:  31.10.2016
  * Opis pliku:             Glowny kotroler aplikacji
  * Prawa dostepu:          755
  * Wykonawca:              "Merruem" Patryk Kurzeja
  **********************************************************/
  require_once("autoload.php");
  $Controler = new Controler();
  $url = "http://" . $Controler->Settings->get('domain');

  $engine   = new Engine($url);
  $ViewsObj  = new Views($Controler);


  $domain = $Controler->Settings->get("domain");
  $url = "http://" . $domain;

  $pageUrl = '';

  if(isset($_GET['p'])){
    if($_GET['p'] != False && $_GET['p'] != 'admin'){

      $conf = Null;


      if(isset($_GET['s1'])){
        $conf['s1'] = $_GET['s1'];
        $pageUrl = $pageUrl . "/" . $conf['s1'];
      }
      if(isset($_GET['s2'])){
        $conf['s2'] = $_GET['s2'];
        $pageUrl = $pageUrl . "/" . $conf['s2'];
      }
      if(isset($_GET['s3'])){
        $conf['s3'] = $_GET['s3'];
        $pageUrl = $pageUrl . "/" . $conf['s3'];
      }



      $confPage = $ViewsObj->getConf($_GET['p'], $conf);
      $page = $ViewsObj->getName($_GET['p']);

    }
    else if($_GET['p'] == 'admin'){
      // To nie powinno sie nigdy wykonac... Ale lepiej niech jest...
      exit("Cos nie tak z konfigami serwera!");
    }
    else{
      $page = 'home';
      $confPage = $ViewsObj->getConf('home');
    }
  }
  else{
    $page = 'home';
    $confPage = $ViewsObj->getConf('home');
  }

  // Wyswietlenie tresci
  $pageUrl = $page . $pageUrl;
  $Controler->Statistics->run($pageUrl);
  $engine->display($page, $confPage);

?>
