<?php

/*********************************************************
* Autor:                  Patryk Kurzeja, Patryk Hansz
* Data utworzenia pliku:  31.10.2016
* Opis pliku:             Plik scalajacy kod aplikacji
* Prawa dostepu:          755
* Wykonawca:              "Merruem" Patryk Kurzeja
**********************************************************/
    $dir = dirname(__FILE__);
    $class =  $dir . "/system/class/";

    ini_set('browscap', $dir . '/system/php_browscap.ini');

    // Zawsze na poczatku
    require_once($class . "db.class.php");
    require_once($dir . "/system/Smarty.class.php");
    require_once($dir . "/admin/class/admin.class.php");

    // Klasy aplikacji
    require_once($class . "news.class.php");
    require_once($class . "settings.class.php");
    require_once($class . "menu.class.php");
    require_once($class . "comments.class.php");
    require_once($class . "pages.class.php");
    require_once($class . "views.class.php");
    require_once($class . "logs.class.php");
    require_once($class . "statistics.class.php");
    require_once($class . "files.class.php");
    require_once($class . "mail.class.php");
    require_once($class . "messages.class.php");
    require_once($class . "newsletter.class.php");

    // To zawsze na końcu
    require_once($class . "controler.class.php");
    require_once($class . "engine.class.php");

  ?>
