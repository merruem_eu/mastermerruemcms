<?php

/*********************************************************
* Autor:                  Patryk Kurzeja
* Data utworzenia pliku:  02.10.2016
* Opis pliku:             Ustawienia strony
* Prawa dostepu:          755
* Wykonawca:              "Merruem" Patryk Kurzeja
**********************************************************/

class Settings{

  private $db;

  public function __construct(){
    $this->db = new Database();
  }
  
  public function get($name){
    try{
      $sql = "SELECT * FROM settings WHERE name = '$name'";
      $resp = $this->db->query($sql);
      if($resp)
        return $resp->value;
      else return False;
    }
    catch(Exception $e){
      return False;
    }
  }
}
 ?>
