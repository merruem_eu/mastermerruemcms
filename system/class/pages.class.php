<?php
  /*********************************************************
  * Autor:                  Patryk Hansz
  * Data utworzenia pliku:  01.10.2016
  * Opis pliku:             Klasa podstron
  * Prawa dostepu:          755
  * Wykonawca:              "Merruem" Patryk Kurzeja
  **********************************************************/
  class Pages{

  public $db;

  public function __construct(){
  		$this->db = new Database();
  	}

	public function getData($pid){
    try{
      $sql = "SELECT * FROM pages WHERE pid = $pid";
      $resp = $this->db->query($sql);
      if($resp)
        return $resp;
      else return False;
    }
    catch(Exception $e){
		return False;
		}
	}
  public function getAll(){
    $sql = "SELECT pages.active, pages.pid, pages.uid, pages.title,
                   pages.content, users.name, users.lastname,
                   users.login, users.email
            FROM pages
            INNER JOIN users ON pages.uid = users.uid";
    $resp = $this->db->queryGetList($sql);
    return $resp;
  }

  public function getQuantity($qnt){
    $users = new Admin();
    try{
      $sql = "SELECT * FROM pages DESC LIMIT".$qnt;
      $resp = $this->db->queryGetList($sql);
      return $resp;
    }
    catch(Exception $e){
      return False;
    }
  }
}

?>
