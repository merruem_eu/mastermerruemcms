<?php

class Controler{
    public $News;
    public $Pages;
    public $Statistics;
    public $Menu;
    public $Settings;
    public $Comments;

    public function __construct(){
      $this->News       = new News();
      $this->Pages      = new Pages();
      $this->Statistics = new Statistics();
      $this->Menu       = new Menu();
      $this->Settings   = new Settings();
      $this->Comments   = new Comments();
    }
}

?>
