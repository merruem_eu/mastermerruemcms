<?php

/*********************************************************
* Autor:                  Patryk Kurzeja
* Data utworzenia pliku:  04.10.2016
* Opis pliku:             Zbieranie statystyk
* Prawa dostepu:          755
* Wykonawca:              "Merruem" Patryk Kurzeja
**********************************************************/

class Statistics{
  private $db;

  public function __construct(){
    $this->db = new Database();
  }

  private function getBrowser(){
    $dir = dirname(dirname(__FILE__));
    if (get_cfg_var('browscap'))
      $browser=get_browser(); //If available, use PHP native function
    else
    {
      require_once($dir . '/php-local-browscap.php');
      $browser=get_browser_local();
    }
    return $browser;
  }

  private function newCount($id, $type){
    $sql = "INSERT INTO stat_counts (count_id, type, count)
            VALUES ('$id', '$type', 1)";
    $this->db->queryNoFetch($sql);
  }

  public function getCount($id){
    $sql = "SELECT * FROM stat_counts WHERE count_id = '$id';";
    $ret = $this->db->query($sql);
    if(!$ret || !isset($ret->count)) return 0;
    else return $ret->count;
  }

  private function updateCount($id, $count){
    $sql = "UPDATE stat_counts
            SET count = $count
            WHERE count_id = '$id'";
    $this->db->queryNoFetch($sql);
  }

  public function incrementCount($id, $type){
      $count = $this->getCount($id);
      if(!$count){
        $this->newCount($id, $type);
      }
      else{
        $count += 1;
        $this->updateCount($id, $count);
      }
      return True;
  }


  public function save($name, $hasz, $page, $newSession){
    $date = date('H-d-m-Y');
    $browserData = $this->getBrowser(null,true);
    $browser     = $browserData->browser;
    $platform    = $browserData->platform;

    $this->incrementCount($browser, 'browser');
    $this->incrementCount($platform,'platform');
    $this->incrementCount($date,    'date');
    $this->incrementCount($page,    'page');
    $this->incrementCount($hasz,    'reloads');
    if($newSession)
      $this->incrementCount($name,  'visits');
  }

  public function run($page){
    if(!isset($_COOKIE['id_sesji'])){
        $hasz = 'ses_' . md5(time());
        setcookie('id_sesji', $hasz, time()+(3600 * 30));
        $newSession = True;
    }else{
      $hasz = $_COOKIE['id_sesji'];
      $newSession = False;
      setcookie('id_sesji', $hasz, time()+(3600 * 30));
    }

    if(!isset($_COOKIE['name'])){
        $name = 'name_' . md5(time()+12);
        setcookie('name', $name, time()+(3600 * 60 * 24 * 60));
    }else{
      $name = $_COOKIE['name'];
      setcookie('name', $name, time()+(3600 * 30 * 24 * 60));
    }

    $this->save($name, $hasz, $page, $newSession);


  }
}
 ?>
