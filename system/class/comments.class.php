<?php
  /*********************************************************
  * Autor:                  Patryk Hansz
  * Data utworzenia pliku:  06.10.2016
  * Opis pliku:             Klasa komentarzy
  * Prawa dostepu:          755
  * Wykonawca:              "Merruem" Patryk Kurzeja
  **********************************************************/
  class Comments{

	private $db;

	public function __construct(){
		$this->db = new Database();
	}

	public function save($data){
		$sql = "INSERT INTO comments (content, tdate, autor, accepted)
            VALUES (
              ".$data['content'].",
              ".$data['tdate'].",
              ".$data['autor'].",
              ".$data['accepted'].");";
		$this->db->query($sql);
		return True;
	}

  public function getData($cid){
    try{
      $sql = "SELECT * FROM comments WHERE cid = '$cid'";
      $resp = $this->db->query($sql);
      if($resp)
        return $resp;
      else return False;
    }
    catch(Exception $e){
      return False;
    }
  }

  public function getQuantity($qnt){
    try{
      $sql = "SELECT * FROM comments ORDER BY tdate DESC LIMIT ".$qnt;
      $resp = $this->db->queryGetList($sql);
      return $resp;
    }
    catch(Exception $e){
      return False;
    }
  }
}


?>
