<?php
/*********************************************************
* Autor:                  Patryk Kurzeja, Patryk Hansz
* Data utworzenia pliku:  31.10.2016
* Opis pliku:             Genrownaie stron
* Prawa dostepu:          755
* Wykonawca:              "Merruem" Patryk Kurzeja
**********************************************************/
class Views{
  private $controler;

  public function __construct($controler){
    $this->constroler = $controler;
  }
  // klasy podstron, genrujace dane na podstrony
  private function home($conf){
    $qnt = $this->constroler->Settings->get('QuantityNewsInHome');
    if($conf){
      $tab = $conf['s1'];
      $start = (( $tab - 1 ) * $qnt) + 1;

    }else{
      $tab = 1;
      $start = 0;
    }

    $news = $this->constroler->News->getQuantity($qnt, $start);
    $ret['news'] = $news;
    $ret['tab'] = $tab;
    $ret['tabs']  = $this->constroler->News->getNumTabs($qnt);
    return $ret;
  }

  private function news($conf){
	$data['news'] = $this->constroler->News->getData($conf['s1']);
	$db = new Database();
	$sql = "SELECT * FROM users WHERE uid = {$data['news']->uid}";
	$resp = $db->query($sql);
	$data['news']->autor = $resp->name . " " . $resp->lastname;
	return $data;
  }

  private function page($conf){
	$data['page'] = $this->constroler->Pages->getData($conf['s1']);
	return $data;
  }

  // Zwraca wszystkie zmienne dotyczace danej podstrony
  public function getConf($pageName, $conf = 0){
    //trzeba tu cos madrego wymyslic
    if($pageName == 'home'){
        $conf = $this->home($conf);
    }

	   else if($pageName == 'news'){
		     $conf = $this->news($conf);
	  }

	  else if($pageName == 'page'){
			$conf = $this->page($conf);
	}

    $conf['PageTitle'] = $this->constroler->Settings->get('PageTitle');
    $conf['PageName'] = $this->constroler->Settings->get('PageName');
    return $conf;
  }

  public function getName($page){
    if($page == 'home') return 'home';
	  elseif($page == 'news') return 'news';
    elseif($page = 'page') return 'page';
    else return 'home';
  }


}

?>
