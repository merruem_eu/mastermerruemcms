<?php
  /*********************************************************
  * Autor:                  Patryk Hansz
  * Data utworzenia pliku:  04.11.2016
  * Opis pliku:             Klasa zakładek w menu
  * Prawa dostepu:          755
  * Wykonawca:              "Merruem" Patryk Kurzeja
  **********************************************************/
class Menu{

  private $db;

  public function __construct(){
		$this->db = new Database();
	}

	public function getData($msid){
    try{
      $sql = "SELECT * FROM menusites WHERE msid = '$msid'";
      $resp = $this->db->query($sql);
      if($resp)
        return $resp;
      else return False;
    }
    catch(Exception $e){
		return False;
		}
	}

  public function getAll(){
    try{
      $sql ="SELECT * FROM menusites";
      return $this->db->queryGetList($sql);
    }catch(Exception $e){
      Logs::add("Blad wywolania menu");
    }
  }

  public function getQuantity($qnt){
    try{
      $sql = "SELECT * FROM menusites ORDER BY msid DESC LIMIT ".$qnt;
      $resp = $this->db->queryGetList($sql);
      return $resp;
    }
    catch(Exception $e){
      return False;
    }
  }

}
?>
