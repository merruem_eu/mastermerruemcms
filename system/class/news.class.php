<?php
  /*********************************************************
  * Autor:                  Patryk Hansz, Patryk Kurzeja
  * Data utworzenia pliku:  01.10.2016
  * Opis pliku:             Klasa newsow
  * Prawa dostepu:          755
  * Wykonawca:              "Merruem" Patryk Kurzeja
  **********************************************************/
  class News{
	public $db;

	public function __construct(){
		$this->db = new Database();
	}

  public function getData($nid){
    try{
      $sql = "SELECT * FROM news WHERE nid = $nid";
      $resp = $this->db->query($sql);
      if($resp)
        return $resp;
      else return False;
    }
    catch(Exception $e){
      return False;
    }
  }
  public function getAll(){
    $sql = "SELECT news.active, news.nid, news.uid, news.title,
                   news.content, news.short_content, users.name, users.lastname,
                   users.login, users.email, news.uid, news.tdate
            FROM news
            INNER JOIN users ON news.uid = users.uid";
    $resp = $this->db->queryGetList($sql);
    return $resp;
  }
  public function getNumTabs($qnt){
    $sql = "SELECT COUNT(*) FROM news WHERE active = 1";
    $ret = $this->db->queryNoFetch($sql);
    $ret = $ret->fetch();
    $ret = $ret['COUNT(*)'];
    return ceil($ret / $qnt);
  }
  public function getQuantity($qnt, $start = 0){
    try{
      $stop = $qnt + $start - 1;
      $sql = "SELECT news.active, news.nid, news.uid, news.title,
                     news.content, news.short_content, users.name, users.lastname,
                     users.login, users.email, news.uid, news.tdate
             FROM news
             INNER JOIN users ON news.uid = users.uid
             WHERE news.nid <= $stop ";
       if($start){
       $sql = $sql . "AND news.nid >= $start ";
      }
      $sql = $sql . "ORDER BY tdate";

      //exit($sql);
      $resp = $this->db->queryGetList($sql);
      return $resp;
    }
    catch(Exception $e){
      return False;
    }
  }
}

?>
