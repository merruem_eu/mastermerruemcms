<?php
/*********************************************************
* Autor:                  Patryk Kurzeja
* Data utworzenia pliku:  02.10.2016
* Opis pliku:             Komunikacja z baza
* Prawa dostepu:          755
* Wykonawca:              "Merruem" Patryk Kurzeja
**********************************************************/
class Database{
  private $pdo;
  private $conf;

  // odczyt configow z pliku
  public function __construct(){
    $dir = dirname(dirname(dirname(__FILE__)));
    try{
      $filename = $dir . "/config.conf";
      if ( !file_exists($filename) ) {
        throw new Exception('File not found.');
      }
      $dane = fread(fopen($filename, "r"), filesize($filename));
      if ( !$dane ) {
        throw new Exception('File open failed.');
      }
      $conf = unserialize($dane);
      $this->conf = $conf['database'];
    }catch(Exception $e){
        //Logs::add("Nie udało się otwirzyć pliku konfiguracyjnego. " . $filename . ' ->'.$e->getMessage());
        throw new Exception('Blad otwarcia pliku konfiguracyjnego: '. $filename . ' ->'.$e->getMessage());
    }
  }

  public function getName(){
    return $this->conf['name'];
  }
  // Metoda nawiazuje polaczenie z baza danych
    private function connect(){
      try{
        $conf = $this->conf;
        $this->pdo = new PDO('mysql:host='.$conf['host'].';dbname='.$conf['name'].';charset=utf8', $conf['user'], $conf['pass'],
  		                        array(
  			                          // wyłączenie zbędnego emulate prepares
  			                          PDO::ATTR_EMULATE_PREPARES => false,
  			                          // ustalenie sposobu raportowania błędów
  			                          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
  	    ));
      }catch(PDOException $err) {
        //Logs::add('Blad polaczenia z baza danych: '.$err->getMessage());
        throw new Exception('Blad polaczenia z baza danych: '.$err->getMessage());
      }
      return 0;
    }

    public function queryNoFetch($ask){
      try{
        $this->connect();
        $zap = $this->pdo->query($ask);
        return $zap;
      }catch(PDOException $err){
        //Logs::add('Blad polaczenia z baza danych: '.$err->getMessage().'</br>SQL: '. $ask);
        throw new Exception('Blad polaczenia z baza danych: '.$err->getMessage().'</br>SQL: '. $ask);
      }
    }

    // Metoda wysyla zapytanie do bazy danych
    public function query($ask){
      $zap = $this->queryNoFetch($ask);
      return $zap->fetch(PDO::FETCH_OBJ);
    }

    public function queryGetList($ask){
      $resp = $this->queryNoFetch($ask);
      $i = 0;
      $dane = array();
      if($resp){
        while($row = $resp->fetch(PDO::FETCH_OBJ)){
          $dane[$i] = $row;
          $i++;
        }
        return $dane;
      }else return array();
    }
}

 ?>
