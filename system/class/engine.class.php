<?php
/*********************************************************
* Autor:                  Patryk Kurzeja, Patryk Hansz
* Data utworzenia pliku:  31.10.2016
* Opis pliku:             Klasa glowna silnika aplikacji
* Prawa dostepu:          755
* Wykonawca:              "Merruem" Patryk Kurzeja
**********************************************************/

class Engine{

  private $smarty;
  private $conf;
  public $url;

  public function __construct($url){
    $this->smarty = new Smarty();
    $themeDir = dirname(dirname(dirname(__FILE__))).'/theme';
    $this->url = $url;
    $this->smarty->setTemplateDir($themeDir);
    $this->smarty->setCompileDir($themeDir.'/theme_c');
    $this->smarty->setCacheDir($themeDir.'/cache');
    $this->smarty->setConfigDir($themeDir.'/configs');

    try{
      $filename = "config.conf";
      if ( !file_exists($filename) ) {
        throw new Exception('File not found.');
      }
      $dane = fread(fopen($filename, "r"), filesize($filename));
      if ( !$dane ) {
        throw new Exception('File open failed.');
      }
      $conf = unserialize($dane);
      $conf['database'] = 0; // usuniecie konfigow bazy dla bezpieczenstwa
      $this->conf = $conf;
    }catch(Exception $e){
        exit('Blad otwarcia pliku konfiguracyjnego: '.$e->getMessage());
    }

    if($conf['debugMode']){
      error_reporting(E_ALL);
      ini_set('display_errors', 1);
    }
    else{
      error_reporting(0);
      ini_set('display_errors', 0);
    }
  }

  // Wyswietlanie danej podstrony, fukcja ktora laczy php z html :P
  public function display($pageName, $config = False){
    if($config)
      while( list($key, $walue) = each($config) )
        $this->smarty->assign($key, $walue);
    $menus = new Menu();
    $this->smarty->assign("menu", $menus->getAll());
    $this->smarty->assign('theme', $this->url . '/theme/');
    $this->smarty->display($pageName.'.tpl');
  }

}

?>
