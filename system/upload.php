<?php
error_reporting(E_ALL); // poziom raportowania, http://pl.php.net/manual/pl/function.error-reporting.php
ini_set('display_errors', 1);

include_once("class/db.class.php");
include_once("class/files.class.php");
include_once("class/logs.class.php");

$Files = new Files();

/* utworzenie zmiennych */
$folder_upload=dirname(dirname(__FILE__)) . "/uploads";
$file_name=$_FILES['plik']['name'];
$file_location=$_FILES['plik']['tmp_name']; //tymczasowa lokalizacja pliku
$plik_mime=$_FILES['plik']['type']; //typ MIME pliku wysłany przez przeglądarkę
$file_size=$_FILES['plik']['size'];
$file_error=$_FILES['plik']['error']; //kod błędu

/* sprawdzenie, czy plik został wysłany */
if (!$file_location) {
	exit("Nie wysłano żadnego pliku");
}

/* sprawdzenie błędów */
switch ($file_error) {
	case UPLOAD_ERR_OK:
		break;
	case UPLOAD_ERR_NO_FILE:
		exit("Brak pliku.");
		break;
	case UPLOAD_ERR_INI_SIZE:
	case UPLOAD_ERR_FORM_SIZE:
		exit("Przekroczony maksymalny rozmiar pliku.");
		break;
	default:
		exit("Nieznany błąd.");
		break;
}

/* sprawdzenie rozszerzenia pliku - dzięki temu mamy pewność, że ktoś nie zapisze na serwerze pliku .php */
$allow_extensions=array("jpeg", "jpg", "tiff", "tif", "png", "gif", "doc", "pdf");
$image_extensions=array("jpeg", "jpg", "tiff", "tif", "png", "gif");

$file_extentions=pathinfo(strtolower($file_name), PATHINFO_EXTENSION);
if (!in_array($file_extentions, $allow_extensions, true)) {
	exit("Niedozwolone rozszerzenie pliku.");
}



$date = date("Ymd");
if(!is_dir($folder_upload."/".$file_extentions)){
	mkdir($folder_upload."/".$file_extentions, 0777, True);
}
$file_newname = $date . "_" . md5($file_name) . "." . $file_extentions;
$file_new = $folder_upload."/".$file_extentions."/".$file_newname;
/* przeniesienie pliku z folderu tymczasowego do właściwej lokalizacji */
if (!move_uploaded_file($file_location, $file_new)) {
	Logs::add("Nie udało się przenieść pliku. Lokalizacja:" . $file_new, $_COOKIE['uid']);
	exit("Nie udało się przenieść pliku. Lokalizacja:" . $file_new);
}

if (in_array($file_extentions, $image_extensions, true)) {
	list($width_orig, $height_orig) = getimagesize($file_new);
	$ratio_orig = $width_orig/$height_orig;
	$width = array(200, 350);
	$height = array(200, 350);
	$widthC = $width;
	$heightC = $height;
	for($i = 0; $i<2; $i++){
		if ($width[$i]/$height[$i] > $ratio_orig) {
   		$width[$i] = $height[$i]*$ratio_orig;
		} else {
   		$height[$i] = $width[$i]/$ratio_orig;
		}
		$image_p = imagecreatetruecolor($width[$i], $height[$i]);
		$image = imagecreatefromjpeg($file_new);
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width[$i], $height[$i], $width_orig, $height_orig);
		$resizname = $date . "_" . md5($file_name) . "_".$widthC[$i]."x".$heightC[$i]."_." . $file_extentions;
		$resize_file = $folder_upload."/".$file_extentions."/". $resizname;
		imagejpeg($image_p, $resize_file, 100);
		Logs::add("Zaladowano plik: ".$resizname, $_COOKIE['uid']);
		$Files->add(array(
			"name" => $resizname,
			"location" => $file_extentions . "/",
			"server_location" => $resize_file,
			"mother_image" => $file_newname,
			"size" => $widthC[$i]."x".$heightC[$i]
		));

	}
	Logs::add("Zaladowano plik: ".$file_newname, $_COOKIE['uid']);
	$Files->add(array(
		"name" => $file_newname,
		"location" => $file_extentions . "/",
		"server_location" => $file_new,
		"mother_image" => $file_newname,
		"size" => $width_orig."x".$height_orig
	));
}else{
	Logs::add("Zaladowano plik: ".$file_newname, $_COOKIE['uid']);
	$Files->add(array(
		"name" => $file_newname,
		"location" => $file_extentions . "/",
		"server_location" => $file_new,
	));
}
// Dodanie informacji o pliku do bazy



/* nie było błędów */
echo "Plik został zapisany.";
?>
