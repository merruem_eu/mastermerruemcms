<?php

/*********************************************************
* Autor:                  Patryk Kurzeja
* Data utworzenia pliku:  02.10.2016
* Opis pliku:             Klasa uzytkownika panelu admina
* Prawa dostepu:          755
* Wykonawca:              "Merruem" Patryk Kurzeja
**********************************************************/


class Admin{
  private $id;
  private $DB;

  public function __construct(){
    //tworzenie obiektu bazy danych
    $this->DB = new Database();
  }

  public function add($data){
    $name = $data['name'];
    $lastname = $data['lastname'];
    $email = $data['email'];
    $login = $data['login'];
    $settings = new Settings();
    $md5pass = md5($settings->get('sole') . $data['pass']);
    $sql = "INSERT INTO users
    (name, lastname, email, login, md5pass)
    VALUES ('$name', '$lastname', '$email', '$login', '$md5pass')";
    $this->db->queryNoFetch($sql);
    Logs::add("Dodano uzytkownia -> ".$name." ".$lastname." - ".$login, $_COOKIE['uid']);
  }

  private function update($key, $time, $uid){
    $sql = "UPDATE logins SET utime = $time WHERE ukey = '$key';";
    setcookie('session_key', $key, time() + 60 * 15, "/");
    setcookie('uid', $uid, time() + 60 * 15, "/");
    $this->DB->queryNoFetch($sql);

  }

  public function verify($key){
    $minTime = time() - (15 * 60);

    $sql = "DELETE FROM logins WHERE utime < $minTime;";
    $this->DB->queryNoFetch($sql);
    $sql = "SELECT * FROM logins WHERE ukey = '$key';";
    $ret = $this->DB->query($sql);
    if($ret){
        $this->update($key, time(), $ret->uid);
        return True;
    }else return False;
  }

  public function getAllUsers(){
    $sql = "SELECT * FROM users;";
    $resp = $this->DB->queryGetList($sql);
    return $resp;
  }

  public function get($uid){
    $sql = "SELECT * FROM users WHERE uid = '$uid'";
    $ret = $this->DB->query($sql);
    if($ret){
      return $ret;
    }else return Null;
  }

  public function getLoginUserData($key){
    $sql = "SELECT * FROM logins WHERE ukey = '$key';";
    $ret = $this->DB->query($sql);
    if($ret){
      $uid = $ret->uid;
      return $this->get($uid);
    }else return Null;
  }

  public function login($user, $pass, $remb){
    $settings = new Settings();
    $sole = $settings->get('sole');
    $md5pass = md5($sole . $pass);
    $sql = "SELECT * FROM users WHERE login = '$user';";
    $ret = $this->DB->query($sql);
    if($ret){
      if($ret->md5pass == $md5pass && $ret->active){
        $hasz = md5(time() + rand(0,10000));
        $time = time();
        $uid = $ret->uid;
        $sql = "INSERT INTO logins (ukey, uid, utime) VALUES ('$hasz', $uid, $time);";
        $this->DB->queryNoFetch($sql);
        $this->update($hasz, $time);
        AdminLogs::add("Uzytkownik zostal zalogowany.", $uid);
        return Null;
      }return "Podano błędne hasło.";
    }return "Nie ma takiego użytkownika";
  }

  public function logout($key){
    $user = $this->getLoginUserData($key);
    setcookie('session_key', 0, time()-1, '/');
    $sql = "DELETE FROM logins WHERE ukey = '$key'";
    $this->DB->queryNoFetch($sql);
    AdminLogs::add("Uzytkownik wylogowal sie.", $user->uid);
  }

  public function logoutAll($uid){
    $user = $this->getLoginUserData($_COOKIE['session_key']);
    $sql = "DELETE FROM logins WHERE uid = '$uid'";
    $this->DB->queryNoFetch($sql);
    AdminLogs::add("Uzytkownik (uid: ".$uid.") zostal awaryjnie wylogowany.", $user->uid);
  }
}


 ?>
