<?php
class AdminNews extends News{

  public function add($data){
    $sql = "INSERT INTO news (title, content, short_content, tdate, active, uid)
            VALUES (
              '".$data['title']."',
              '".$data['content']."',
              '".$data['short_content']."',
              NOW(),
              1,
              ".$_COOKIE['uid'].")";
    $this->db->queryNoFetch($sql);
    return True;
  }
  public function edit($data){
    $sql = "UPDATE news
            SET title = '".$data['title']."',
                content='".$data['content']."',
                short_content = '".$data['short_content']."'.
                uid='".$data['uid']."'
            WHERE nid='".$data['nid']."'";
    $this->db->queryNoFetch($sql);
  }
  public function toogleVisible($nid){
    try{
      $sql = "SELECT * FROM news WHERE nid='$nid'";
      $page = $this->db->query($sql);
      $vis = $page->active;
      if($vis) $newVis = 0; else $newVis = 1;
      $sql = "UPDATE news SET active = $newVis WHERE nid = $nid";
      $this->db->queryNoFetch($sql);
    }
      catch(Exception $e){
        Logs::add("Wystapił blad zmiany widocznosci newsa: ".$e->getMessage(), $_COOKIE['uid']);
        return False;
      }
    Logs::add("Zmieniono widocznosc newsa o id: ". $nid, $_COOKIE['uid']);
    return $vis;
  }


  public function remove($nid){
    try{
        $sql = "DELETE FROM news WHERE nid='$nid'";
        $this->db->query($sql);
      }
      catch(Exception $e){
        Logs::add('Wystapil nieoczekiwany blad przy usuwaniu wpisu: ' . $e->getMessage(), $_COOKIE['uid']);
        return False;
      }
    Logs::add('Usunieto wpis o id: ' . $nid, $_COOKIE['uid']);
    return True;
  }
}
 ?>
