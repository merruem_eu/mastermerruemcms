<?php
class AdminViews{
  public static function pages($s1, $s2, $s3, $smarty){
    $pages = new AdminPages();
    if($s1 == 'new'){
      $smarty->assign("action", '/admin/pages/add');
      return "pageform";
    }else if($s1 == 'add'){
      $pages->add($_POST);
    }else if($s1 == 'save'){
      $pages->edit($_POST);
    }else if($s1 == 'toogle'){
      $pages->toogleVisible($_POST['pid']);
    }else if($s1 == 'del'){
      $pages->remove($_POST['pid']);
    }else if($s1 == 'edit'){
      $page = $pages->getData($_POST['pid']);
      $smarty->assign("page", $page);
      $smarty->assign("action", '/admin/pages/save');
      return "pageform";
    }
    $smarty->assign("pagesList", $pages->getAll());
    return "pages";
  }

public static function news($s1, $s2, $s3, $smarty){
    $news = new AdminNews();
    if($s1 == 'new'){
      $smarty->assign("action", '/admin/news/add');
      return "newsForm";
    }else if($s1 == 'add'){
      $news->add($_POST);
    }else if($s1 == 'save'){
      $wert = 0;// Zapisanie edycji'
    }else if($s1 == 'toogle'){
      $news->toogleVisible($_POST['nid']);
    }else if($s1 == 'del'){
      $news->remove($_POST['nid']);
    }else if($s1 == 'edit'){
      $news = $news->getData($_POST['nid']);
      $smarty->assign("news", $news);
      $smarty->assign("action", '/admin/news/save');
      return "newsForm";
    }
    $smarty->assign("newsList", $news->getAll());
    return "news";
  }

  public static function statistics($s1, $s2, $s3, $smarty){
    $stat = new Analyze();
    if($s1 == 'soft'){
      $browserStat = $stat->getAllCountArray('browser');
      $osStat = $stat->getAllCountArray('platform');

      $smarty->assign('browserStat', $browserStat);
      $smarty->assign('osStat', $osStat);
      $smarty->assign('browserStatCount', count($browserStat));
      $smarty->assign('osStatCount', count($osStat));
      return "statsoft";
    }else{
      $timeStat = $stat->getCountArray('date', 20);
      $pageStat = $stat->getAllCountArray('page');
      $sesions = $stat->sumOfAllRecords('visits');
      $reloads = $stat->sumOfAllRecords('reloads');

      $smarty->assign('sesions', $sesions);
      $smarty->assign('reloads', $reloads);
      $smarty->assign('timeStat', $timeStat);
      $smarty->assign('pageStat', $pageStat);
      $smarty->assign('timeStatCount', count($timeStat));
      $smarty->assign('pageStatCount', count($pageStat));
      return "stat";
    }
  }

  public static function info($s1, $s2, $s3, $smarty){
    return "info";
  }

  public static function messages($s1, $s2, $s3,$smarty){
      $mess = new AdminMessages();
      $messList = $mess->getAll();
      $smarty->assign("messagesList", $messList);
      return "messages";
  }

  public static function home($s1, $s2, $s3,$smarty){
    $stat = new Analyze($s1, $s2, $s3, $smarty);
    $mess = new AdminMessages();
    $cmess = $mess->count(1,0);
    $alrt = $mess->count(1,1);
    $timeStat = $stat->getCountArray('date', 20);
    $smarty->assign('countMessages', $cmess);
    $smarty->assign('countAlerts', $alrt);
    $smarty->assign('timeStat', $timeStat);
    $smarty->assign('timeStatCount', count($timeStat));
    return 'home';
  }

  public static function user($s1, $s2, $s3,$smarty){
    $admin = new Admin();
    if($s1){
      $user = $admin->get($s1);
    }else{
      $user = $admin->getLoginUserData($_COOKIE['session_key']);
    }

    if(isset($_POST['option'])){
      if($_POST['option'] == 'out') $admin->logoutAll($s1);
    }

    $smarty->assign('user', $user);
    return "user";
  }

  public static function users($s1, $s2, $s3,$smarty){
    $admin = new Admin();
    $users = $admin->getAllUsers();
    $smarty->assign('usersList', $users);
    return "users";
  }

  public static function logs($s1, $s2, $s3,$smarty){
    $logs = new AdminLogs();
    $list = $logs->get();
    $smarty->assign('logsList', $list);
    return "logs";
  }

  public static function settings($s1, $s2, $s3,$smarty){
    $settings = new AdminSettings();

    if($s1 == "add" && isset($_POST['name']) && isset($_POST['value'])){
      $settings->set($_POST['name'], $_POST['value']);
    }
    if($s1 == 'edit' && isset($_POST['name']) && isset($_POST['value'])){
      $settings->edit($_POST['name'], $_POST['value']);
    }
    if($s1 == 'delete' && isset($_POST['name'])){
      $settings->delete($_POST['name']);
    }

    $list = $settings->getAll();
    $smarty->assign('settingsList', $list);
    return "settings";
  }
}
 ?>
