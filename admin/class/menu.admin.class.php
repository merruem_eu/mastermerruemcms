<?php
class AdminMenu extends Menu{
  public function save($data){
    $sql = "INSERT INTO menusites (title, tlink)
            VALUES (
              ".$data['title'].",
              ".$data['tlink'].");";
    $this->db->query($sql);
    return True;
  }

  public function remove($msid){
  //nie dodaję tu automatycznego przesuwania, jeśli ktoś usunie element w środku i zrobi się luka
  //póki co, zakładam że użytkownik albo my zajmiemy się przesuwaniem w innym miejscu używając funkcji swap
    try{
      $sql = "DELETE FROM menusites WHERE msid = '$msid'";
      $this->db->query($sql);
      }
      catch(Exception $e){
        return False;
      }
    return True;
  }

  public function swap($msid, $newmsid){
  //zamienia dane msid i newmsid miejscami w tabeli (kolejnością na pasku)
  try{
    $data1 = $this->getData($msid);
    $data2 = $this->getData($newmsid);

    $this->remove($msid);
    $this->remove($newmsid);

    $temp = $data1['msid'];
    $data1['msid'] = $data2['msid'];
    $data2['msid'] = $temp;

    $this->save($data1);
    $this->save($data2);
      }
      catch(Exception $e){
        return False;
      }
    return True;
  }

}
?>
