<?php
class AdminMessages extends Messages{
  public function getAll($onlyUnopen = 0){
    try{
      $sql = "SELECT * FROM messages";
      if($onlyUnopen) $sql = $sql . " WHERE opened = 0";
      return $this->db->queryGetList($sql);
    }catch(Exception $e){
      Logs::add('Wystapil blad przy ladowniu wiadomosci ', $_COOKIE['uid']);
    }
  }

  public function get($mid){
    try{
      $sql = "SELECT * from messages WHERE pid = '$mid'";
      return $this->db->query($sql);
    }catch(Exception $e){
      Logs::add("Wystapil blad przy ladowniu wiadomosci.", $_COOKIE['uid']);
    }
  }

  public function open($mid){
    try{
      $sql = "UPDATE messages SET opened = 1 WHERE mid = $mid";
      $this->db->query($sql);
    }catch(Exception $e){
      Logs::add("Nie udalo sie zmienic atrybutu wiadomosci", $_COOKIE['uid']);
    }
  }

  public function count($onlyUnopen = 0, $onlyAlerts = 0){
    try{
      $bo = 0;
      $sql = "SELECT * FROM messages";
      if($onlyUnopen || $onlyAlerts) $sql = $sql . " WHERE ";
      if($onlyUnopen) {$sql = $sql . "opened = 0"; $bo = 1;}
      if($bo && $onlyAlerts) $sql = $sql . " and ";
      if($onlyAlerts) $sql = $sql . "alerts = 1";
      $resp = $this->db->queryNoFetch($sql);
      if($resp){
        $i = 0;
        while($row = $resp->fetch(PDO::FETCH_OBJ)) $i++;
        return $i;
      }else return 0;
    }catch(Exception $e){
      if(isset($_COOKIE['uid'])) Logs::add("Nie udalo sie zliczyc wiadomosci", $_COOKIE['uid']); Logs::add("Nie udalo sie zliczyc wiadomosci");

    }
  }


}
 ?>
