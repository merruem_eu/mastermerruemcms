<?php

class AdminControler{
    public $News;
    public $Pages;
    public $Analyze;
    public $Menu;
    public $Settings;
    public $Comments;
    public $Logs;

    public function __construct(){
      $this->News     = new AdminNews();
      $this->Pages    = new AdminPages();
      $this->Analyze  = new Analyze();
      $this->Menu     = new AdminMenu();
      $this->Settings = new AdminSettings();
      $this->Comments = new AdminComments();
      $this->Logs     = new AdminLogs();
    }
}

?>
