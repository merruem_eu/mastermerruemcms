<?php
  class AdminLogs extends Logs{
    public $db;
    public function __construct(){
      $this->db = new Database();
    }

    public function get(){
      $users = new Admin();
      $sql = "SELECT * FROM logs ORDER BY logid DESC";
      $resp = $this->db->queryGetList($sql);
      return $resp;
    }
  }
?>
