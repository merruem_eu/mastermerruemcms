<?php

/*********************************************************
* Autor:                  Patryk Kurzeja
* Data utworzenia pliku:  05.10.2016
* Opis pliku:             Klasa przetwarzajaca dane statystyczne
* Prawa dostepu:          755
* Wykonawca:              "Merruem" Patryk Kurzeja
**********************************************************/

class Analyze{
  private $db;
  public function __construct(){
    $this->db = new Database();
  }

  public function getCountArray($type, $num){
    $sql = "SELECT * FROM stat_counts WHERE type = '$type';";
    $resp = $this->db->queryGetList($sql);
    return array_reverse($resp);
  }

    public function getAllCountArray($type){
      $sql = "SELECT * FROM stat_counts WHERE type = '$type';";
      $resp = $this->db->queryGetList($sql);
      return array_reverse($resp);
    }

    public function countRecords($type){
      $sql = "SELECT * FROM stat_counts WHERE type = '$type';";
      $resp = $this->db->queryNoFetch($sql);
      if($resp){
        $i = 0;
        $dane = Null;
        while($row = $resp->fetch(PDO::FETCH_OBJ)){
          $i++;
        }
        return $i;
      }
      else return False;
    }

    public function sumOfAllRecords($type){
      $sql = "SELECT * FROM stat_counts WHERE type = '$type';";
      $resp = $this->db->queryNoFetch($sql);
      if($resp){
        $i = 0;
        $dane = Null;
        while($row = $resp->fetch(PDO::FETCH_OBJ)){
          $i += $row->count;
        }
        return $i;
      }
      else return False;
    }

}
 ?>
