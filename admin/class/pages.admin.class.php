<?php
class AdminPages extends Pages{

  public function add($data){
    $cont = str_replace("'","/'",$data['content']);
    $sql = "INSERT INTO pages (title, content, uid, active)
            VALUES (
              '".$data['title']."',
              '".$cont."',
              ".$_COOKIE['uid'].", 1)";
    $r = $this->db->queryNoFetch($sql);
    Logs::add("Dodano nowa strone o tytuly: " . $data['title'], $_COOKIE['uid']);
    return True;
  }

  public function edit($data){
    $sql = "UPDATE pages
            SET title = '".$data['title']."',
                content='".$data['content']."',
                uid='".$data['uid']."'
            WHERE pid='".$data['pid']."'";
    $this->db->queryNoFetch($sql);
  }
  public function toogleVisible($pid){
    try{
      $sql = "SELECT * FROM pages WHERE pid='$pid'";
      $page = $this->db->query($sql);
      $vis = $page->active;
      if($vis) $newVis = 0; else $newVis = 1;
      $sql = "UPDATE pages SET active = $newVis WHERE pid = $pid";
      $this->db->queryNoFetch($sql);
    }
      catch(Exception $e){
        Logs::add("Wystapił blad zmiany widocznosci strony: ".$e->getMessage(), $_COOKIE['uid']);
        return False;
      }
    Logs::add("Zmieniono widocznosc strony o id: ". $pid, $_COOKIE['uid']);
    return $vis;
  }

  public function remove($pid){
    try{
      $sql = "DELETE FROM pages WHERE pid='$pid'";
      $this->db->query($sql);
    }
      catch(Exception $e){
        Logs::add("Wystapił blad przy usuwaniu strony: ".$e->getMessage(), $_COOKIE['uid']);
        return False;
      }
    Logs::add("Usunieto strone o id: ". $pid, $_COOKIE['uid']);
    return True;
  }
}

?>
