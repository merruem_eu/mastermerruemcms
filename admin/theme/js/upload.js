$('#FileDanger').hide();
$("#FileSuccess").hide();

$("#sendBt").click(function(){
  $('#FileDanger').hide();
  $("#FileSuccess").hide();
  $("#FileProgress").css("width",0 + "%");
  $("#FileProgressText").html("0%");
});

$("#exitBt").click(function(){
  $('#FileDanger').hide();
  $("#FileSuccess").hide();
  $("#FileProgress").css("width",0 + "%");
  $("#FileProgressText").html("0%");
});

$('#addFile').on('shown.bs.modal', function () {
  $('#addFileButton').focus();
});



function wyslijPlik(webDomain) {
    var plik=document.getElementById("plik").files[0];

    var formularz=new FormData(); //tworzymy nowy formularz do wysłania
    formularz.append("plik", plik); //dodajemy do formularza pole z naszym plikiem

    /* wysyłamy formularz za pomocą AJAX */
    var xhr=new XMLHttpRequest();
    xhr.upload.addEventListener("progress", postepWysylania, false);
    xhr.addEventListener("load", zakonczenieWysylania, false);
    xhr.addEventListener("error", bladWysylania, false);
    xhr.addEventListener("abort", przerwanieWysylania, false);
    xhr.open("POST", "http://" + webDomain + "/system/upload.php", true);
    xhr.send(formularz);
}

function postepWysylania(event) {
    var procent=Math.round((event.loaded/event.total)*100);
    $("#FileProgress").css("width",procent + "%");
    $("#FileProgressText").html("Wysłano "+konwersjaBajtow(event.loaded)+" z "+konwersjaBajtow(event.total)+" ("+procent+"%)");
}

function zakonczenieWysylania(event) {
    if(event.target.responseText != "Plik został zapisany."){
      $('#FileDanger').show();
      $('#FileDanger').html(event.target.responseText);
    }
    else{
      $("#FileSuccess").show();
      $("#FileSuccess").html(event.target.responseText);
    }
}

function bladWysylania(event) {
  $('#FileDanger').show();
  $('#FileDanger').html("Wysyłanie nie powiodło się");
}

function przerwanieWysylania(event) {
  $('#FileDanger').show();
  $('#FileDanger').html("Wysyłanie zostało przerwane");
}

function konwersjaBajtow(bajty) {
    var kilobajt=1024;
    var megabajt=kilobajt*1024;
    var gigabajt=megabajt*1024;
    var terabajt=gigabajt*1024;

    if (bajty>=0 && bajty<kilobajt) return bajty+" B";
    else if(bajty>=kilobajt && bajty<megabajt) return Math.round(bajty/kilobajt)+" kB";
    else if(bajty>=megabajt && bajty<gigabajt) return Math.round(bajty/megabajt)+" MB";
    else if(bajty>=gigabajt && bajty<terabajt) return Math.round(bajty/gigabajt)+" GB";
    else if(bajty>=terabajt) return Math.round(bajty/terabajt)+" TB";
    else return bajty+" B";
}
