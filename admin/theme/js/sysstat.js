function Round(n, k)
{
var factor = Math.pow(10, k+1);
n = Math.round(Math.round(n*factor)/10);
return n/(factor/10);
}

$(document).ready(function () {
  $.ajax({
      type: "GET",
      url: "/system/sysinfo/xml.php?plugin=complete&xml",
      cache: false,
      dataType: "xml",
      success: function(xml) {
          $(xml).find('Hardware').each(function(){
            $(this).find("CPU").each(function(){
              var i = 0;
              $(this).find('CpuCore').each(function(){
                $('.sysstat').append('<li><a href="#"><div><p><strong>Procesor '+i+': </strong><span class="pull-right text-muted"><i class="core'+i+'"></i></span></p><div class="progress progress-striped active"><div class="progress-bar progress-bar-success barCore'+i+'" role="progressbar" " aria-valuemin="0" aria-valuemax="100" style="width: 0%"><span class="sr-only prcCore'+i+'>0%</span></div></div></div></a></li>');
                i++;
              });
            });
          });
      }
  });

  setInterval(function(){
    $.ajax({
        type: "GET",
        url: "/system/sysinfo/xml.php?plugin=complete&xml",
        cache: false,
        dataType: "xml",
        success: function(xml) {
            $(xml).find('Hardware').each(function(){
              $(this).find("CPU").each(function(){
                var i = 0;
                $(this).find('CpuCore').each(function(){
                  var speed = $(this).attr('CpuSpeed');
                  var maxspeed = $(this).attr('CpuSpeedMax');
                  var minspeed = $(this).attr('CpuSpeedMin');
                  var prc = maxspeed - minspeed;
                  prc = Round(((speed - minspeed) / prc ) * 100, 0);
                  $('.core'+i).html(prc + '% ' + speed / 1000 + "GHz");
                  $('.barCore'+i).css('width', prc+'%');
                  $('.prcCore'+i).html(prc+'%');
                  i++;
                });
              });
            });

            $(xml).find('Memory').each(function(){
              $(".totalMem").html(Round( $(this).attr("Total")/(1024*1024*1024) ,2));
              $(".usedMem").html(Round($(this).attr("Used")/(1024*1024*1024),2));
              $(".percMem").html($(this).attr("Percent"));
              $(".barMem").css("width", $(this).attr("Percent")+"%");
            });
        }
    });

  },5000);
});
