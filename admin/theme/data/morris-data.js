$(function() {

    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            Czas: '16:00 01/01/2016',
            zaladowania: 2666,
            wizyty: null
        }, {
            Czas: '17:00 01/01/2016',
            zaladowania: 2778,
            wizyty: 2294
        }, {
            Czas: '18:00 01/01/2016',
            zaladowania: 4912,
            wizyty: 1969
        }, {
            Czas: '19:00 01/01/2016',
            zaladowania: 3767,
            wizyty: 3597
        }, {
            Czas: '20:00 01/01/2016',
            zaladowania: 6810,
            wizyty: 1914
        }, {
            Czas: '21:00 01/01/2016',
            zaladowania: 5670,
            wizyty: 4293
        }, {
            Czas: '22:00 01/01/2016',
            zaladowania: 4820,
            wizyty: 3795
        }, {
            Czas: '23:00 01/01/2016',
            zaladowania: 15073,
            wizyty: 5967
        }],
        xkey: 'Czas',
        ykeys: ['zaladowania', 'wizyty'],
        labels: ['Załadowania', 'Wizyty'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });

    Morris.Donut({
        element: 'morris-pages-chart',
        data: [{
            label: "Download Sales",
            value: 12
        }, {
            label: "In-Store Sales",
            value: 30
        }, {
            label: "Mail-Order Sales",
            value: 20
        }],
        resize: true
    });

    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: '2006',
            a: 100,
            b: 90
        }, {
            y: '2007',
            a: 75,
            b: 65
        }, {
            y: '2008',
            a: 50,
            b: 40
        }, {
            y: '2009',
            a: 75,
            b: 65
        }, {
            y: '2010',
            a: 50,
            b: 40
        }, {
            y: '2011',
            a: 75,
            b: 65
        }, {
            y: '2012',
            a: 100,
            b: 90
        }],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        hideHover: 'auto',
        resize: true
    });

});
