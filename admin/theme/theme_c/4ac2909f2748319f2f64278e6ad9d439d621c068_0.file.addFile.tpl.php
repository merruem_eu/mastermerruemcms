<?php
/* Smarty version 3.1.30, created on 2016-11-19 15:44:53
  from "/var/www/html/admin/theme/parts/modals/addFile.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5830656531d7c9_06521977',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4ac2909f2748319f2f64278e6ad9d439d621c068' => 
    array (
      0 => '/var/www/html/admin/theme/parts/modals/addFile.tpl',
      1 => 1479566687,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5830656531d7c9_06521977 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="addFile" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="fileupload" action="http://<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
/uploads/index.php" method="POST" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Dodanie pliku na serwer</h4>
        </div>
        <div class="modal-body">
          <!-- Redirect browsers with JavaScript disabled to the origin page -->
          <noscript><input type="hidden" name="redirect" value="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin"></noscript>
          <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
          <span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Dodaj plik</span>
            <input type="file" name="files[]" multiple>
          </span>
          <div class="row fileupload-buttonbar">
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
              <!-- The global progress bar -->
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                  <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
              </div>
            </div>
            <table role="presentation" class="table table-striped"><tbody class="files"></tbody>
            </table>
          </div>
          <div class="modal-footer">
            <div class="col-lg-12">
              <!-- The fileinput-button span is used to style the file input field as button -->
              <button type="submit" class="btn btn-primary start">
                  <i class="glyphicon glyphicon-upload"></i>
                  <span>Rozpocznij wgrywanie</span>
              </button>
              <button type="reset" class="btn btn-warning cancel">
                  <i class="glyphicon glyphicon-ban-circle"></i>
                  <span>Anuluj wgrywanie</span>
              </button>
              <button type="button" class="btn btn-danger delete">
                  <i class="glyphicon glyphicon-trash"></i>
                  <span>Usuń</span>
              </button>
              <input type="checkbox" class="toggle">
              <!-- The global file processing state -->
              <span class="fileupload-process"></span>
              <button id="exitBt" type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
          </div>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }
}
