<?php
/* Smarty version 3.1.30, created on 2016-11-19 15:26:44
  from "/var/www/html/admin/theme/parts/nav.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_583061244b3b09_80137972',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '55e26dd1ed369a85208abcccdcff6a53e6a76f76' => 
    array (
      0 => '/var/www/html/admin/theme/parts/nav.tpl',
      1 => 1479565500,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/nav/messages.tpl' => 1,
    'file:parts/nav/task.tpl' => 1,
    'file:parts/nav/notify.tpl' => 1,
  ),
),false)) {
function content_583061244b3b09_80137972 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Nowy CMS</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <?php $_smarty_tpl->_subTemplateRender("file:parts/nav/messages.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <!-- /.dropdown -->
        <?php $_smarty_tpl->_subTemplateRender("file:parts/nav/task.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <!-- /.dropdown -->
        <?php $_smarty_tpl->_subTemplateRender("file:parts/nav/notify.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        </li>
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <?php echo $_smarty_tpl->tpl_vars['loginUser']->value->name;?>
 <?php echo $_smarty_tpl->tpl_vars['loginUser']->value->lastname;?>
 <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
               <li><div style="padding-left: 20px;"><b><?php echo $_smarty_tpl->tpl_vars['loginUser']->value->name;?>
 <?php echo $_smarty_tpl->tpl_vars['loginUser']->value->lastname;?>
</b></div>
               </li>
                <li><a href="/admin/user"><i class="fa fa-user fa-fw"></i> Profil użytkownika</a>
                </li>
                <li><a href="/admin/settings"><i class="fa fa-gear fa-fw"></i> Ustawienia</a>
                </li>
                <li class="divider"></li>
                <li><a href="/admin/login/out"><i class="fa fa-sign-out fa-fw"></i> Wyloguj</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                <li>
                    <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="/admin/stat"><i class="fa fa-bar-chart-o fa-fw"></i> Statystyki<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/stat">Odwiedziny</a>
                        </li>
                        <li>
                            <a href="/admin/stat/soft">Oprogramowanie</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="/admin/news"><i class="fa fa-calendar fa-fw"></i> Newsy<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/news/add">Dodaj nowy</a>
                        </li>
                        <li>
                            <a href="/admin/news">Zarządzaj</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/admin/pages"><i class="fa fa-file-text-o fa-fw"></i> Strony<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/pages/new">Dodaj nową</a>
                        </li>
                        <li>
                            <a href="/admin/pages">Zarządzaj</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/admin/menu"><i class="fa fa-th-list fa-fw"></i> Menu<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/menu/add">Dodaj nową pozycje</a>
                        </li>
                        <li>
                            <a href="/admin/menu">Zarządzaj</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="/admin/users"><i class="fa fa-users fa-fw"></i> Użytkownicy</a>
                </li>
                <li>
                    <a href="/admin/settings"><i class="fa fa-wrench fa-fw"></i> Ustawienia</a>
                </li>
                <li>
                  <a href="/admin/logs"><i class="fa  fa-th-list fa-fw"></i> Logi</a>
                <li>
                    <a href="/admin/info"><i class="fa fa-info-circle fa-fw"></i> Informacje</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
<?php }
}
