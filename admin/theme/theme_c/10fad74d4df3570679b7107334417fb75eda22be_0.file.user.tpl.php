<?php
/* Smarty version 3.1.30, created on 2016-11-19 19:32:05
  from "/var/www/html/admin/theme/pages/user.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58309aa58e5bc2_30572236',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '10fad74d4df3570679b7107334417fb75eda22be' => 
    array (
      0 => '/var/www/html/admin/theme/pages/user.tpl',
      1 => 1479580322,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/head.tpl' => 1,
    'file:parts/modals/selectImage.tpl' => 1,
    'file:parts/modals/addFile.tpl' => 1,
    'file:parts/nav.tpl' => 1,
    'file:parts/scripts.tpl' => 1,
  ),
),false)) {
function content_58309aa58e5bc2_30572236 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:parts/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>
    <?php $_smarty_tpl->_subTemplateRender("file:parts/modals/selectImage.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php $_smarty_tpl->_subTemplateRender("file:parts/modals/addFile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>




    <div id="wrapper">

      <?php $_smarty_tpl->_subTemplateRender("file:parts/nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Informacje o koncie: <?php echo $_smarty_tpl->tpl_vars['user']->value->name;?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->lastname;?>
</h1>
                    </div>
                    <div class="col-lg-5">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Informacje o <?php echo $_smarty_tpl->tpl_vars['user']->value->name;?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->lastname;?>

                        </div>
                        <div class="panel-body">
                          <div class="form-group">
                            <label>Imię i nazwisko:</label>
                            <p class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['user']->value->name;?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->lastname;?>
</p>
                          </div>
                          <div class="form-group">
                            <label>Login:</label>
                            <p class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['user']->value->login;?>
</p>
                          </div>
                          <div class="form-group">
                            <label>Adres email:</label>
                            <p class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['user']->value->email;?>
</p>
                          </div>
                        </div>
                        <div class="panel-footer">
                            <a class="btn btn-primary">Edytuj informacje</a>
                            <a class="btn btn-primary">Zmień hasło</a>
                            <form action="/admin/user/<?php echo $_smarty_tpl->tpl_vars['user']->value->uid;?>
" method="POST">
                              <input type="hidden" name="option" value="out" />
                              <button class="btn btn-danger" type="submit">
                                <i class="fa  fa-power-off"></i> Wyloguj z wszystkich urządzeń</button>
                            </form>
                        </div>
                    </div>
                    </div>

                    <div class="col-lg-5">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Avatar
                        </div>
                        <div class="panel-body">
                          <img src="https://www.fanspole.com/assets/default_user-c283cfbc3d432e22b1d2f1eef515d0b9.png" alt="..." class="img-rounded">
                        </div>
                        <div class="panel-footer">
                          <button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#selectImage">
                            <p class="fa fa-plus"></p> Wybierz plik
                          </button>
                          <button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#addFile">
                            <p class="fa fa-upload"></p> Dodaj plik
                          </button>
                        </div>
                    </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $_smarty_tpl->_subTemplateRender("file:parts/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</body>

</html>
<?php }
}
