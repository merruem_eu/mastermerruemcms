<?php
/* Smarty version 3.1.30, created on 2016-11-19 17:31:17
  from "/var/www/html/admin/theme/LICENSE" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58307e552a6bb2_87699087',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '53fb221eb9ddd528962d23fd37aaa2539f9ff967' => 
    array (
      0 => '/var/www/html/admin/theme/LICENSE',
      1 => 1478469294,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58307e552a6bb2_87699087 (Smarty_Internal_Template $_smarty_tpl) {
?>

The MIT License (MIT)</p>
</br>
<p>Copyright (c) 2013-2016 Blackrock Digital LLC.</p>
</br>
<p>Permission is hereby granted, free of charge, to any person obtaining a copy</br>
of this software and associated documentation files (the "Software"), to deal</br>
in the Software without restriction, including without limitation the rights</br>
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell</br>
copies of the Software, and to permit persons to whom the Software is</br>
furnished to do so, subject to the following conditions:</br>
</p>
</br>
<p>The above copyright notice and this permission notice shall be included in</br>
all copies or substantial portions of the Software.</p>
</br>
<p>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR</br>
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,</br>
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE</br>
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER</br>
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,</br>
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN</br>
THE SOFTWARE.
<?php }
}
