<?php
/* Smarty version 3.1.30, created on 2016-11-19 17:30:03
  from "/var/www/html/admin/theme/pages/stat.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58307e0bedf6f2_52750183',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '10a8edc9034580eb8e23f644fab564895d8913f9' => 
    array (
      0 => '/var/www/html/admin/theme/pages/stat.tpl',
      1 => 1479572616,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/head.tpl' => 1,
    'file:parts/nav.tpl' => 1,
    'file:parts/scripts.tpl' => 1,
  ),
),false)) {
function content_58307e0bedf6f2_52750183 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:parts/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>

    <div id="wrapper">

      <?php $_smarty_tpl->_subTemplateRender("file:parts/nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Statystyki</h1>
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Odwiedziny
                                    <div class="pull-right">

                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div id="morris-area-chart"></div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                          </div>
                            <div class="col-lg-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Strony
                                    <div class="pull-right">
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div id="morris-pages-chart"></div>
                                </div>
                                <!-- /.panel-body -->
                            </div>


                          </div>

                          <div class="col-lg-6">

                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  <i class="fa fa-bar-chart-o fa-fw"></i> Proporcja wizyt do odwiedzajacych
                                  <div class="pull-right">
                                  </div>
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body">
                                  <div id="morris-visits-chart"></div>
                              </div>
                              <!-- /.panel-body -->
                          </div>


                        </div>

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $_smarty_tpl->_subTemplateRender("file:parts/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php echo '<script'; ?>
>$(function() {

        Morris.Area({
            element: 'morris-area-chart',
            data: [
<?php $_smarty_tpl->_assignInScope('i', 0);
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['timeStat']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
          { Czas: '<?php echo $_smarty_tpl->tpl_vars['v']->value->count_id;?>
', odwiedziny: <?php echo $_smarty_tpl->tpl_vars['v']->value->count;?>
 }<?php if ($_smarty_tpl->tpl_vars['i']->value != $_smarty_tpl->tpl_vars['timeStatCount']->value-1) {?>,
  <?php }?>
  <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            ],
            xkey: 'Czas',
            ykeys: ['odwiedziny'],
            labels: ['Wyświetlenia'],
            pointSize: 2,
            hideHover: 'auto',
            resize: true
        });

        Morris.Donut({
            element: 'morris-pages-chart',
            data: [
              <?php $_smarty_tpl->_assignInScope('i', 0);
?>
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pageStat']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
              {
                label: "<?php echo $_smarty_tpl->tpl_vars['v']->value->count_id;?>
",
                value: <?php echo $_smarty_tpl->tpl_vars['v']->value->count;?>

            }<?php if ($_smarty_tpl->tpl_vars['i']->value != $_smarty_tpl->tpl_vars['pageStatCount']->value-1) {?>,
    <?php }?>
    <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);
?>
  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


          ],
            resize: true
        });


        Morris.Donut({
            element: 'morris-visits-chart',
            data: [
              {
                label: "Wizyty",
                value: <?php echo $_smarty_tpl->tpl_vars['sesions']->value;?>

            },{
              label: "Załadownia",
              value: <?php echo $_smarty_tpl->tpl_vars['reloads']->value;?>

            }
          ],
            resize: true
        });
    });
<?php echo '</script'; ?>
>

</body>

</html>
<?php }
}
