<?php
/* Smarty version 3.1.30, created on 2016-11-19 16:49:01
  from "/var/www/html/admin/theme/parts/modals/selectImage.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5830746da24e19_22695647',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '59b51cb063d925c721aa31886876020eef251297' => 
    array (
      0 => '/var/www/html/admin/theme/parts/modals/selectImage.tpl',
      1 => 1479570538,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/fileButton.tpl' => 1,
  ),
),false)) {
function content_5830746da24e19_22695647 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="selectImage" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Wybierz obraz</h4>
        </div>
        <div class="modal-body">
          <div class="Collage">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['imagesList']->value, 'img');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['img']->value) {
?>
              <img id="<?php echo $_smarty_tpl->tpl_vars['img']->value->fid;?>
" src="/uploads/files/<?php echo $_smarty_tpl->tpl_vars['img']->value->location;
echo $_smarty_tpl->tpl_vars['img']->value->name;?>
"/>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

          </div>
        </div>
        <div class="modal-footer">
          <button id="exitBt" type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
          <?php $_smarty_tpl->_subTemplateRender("file:parts/fileButton.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

          <button id="sendBt" type="button" class="btn btn-primary">Wybierz</button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }
}
