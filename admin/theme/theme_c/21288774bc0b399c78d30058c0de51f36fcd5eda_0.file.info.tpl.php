<?php
/* Smarty version 3.1.30, created on 2016-11-19 17:31:17
  from "/var/www/html/admin/theme/pages/info.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58307e5529c0d5_52546925',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '21288774bc0b399c78d30058c0de51f36fcd5eda' => 
    array (
      0 => '/var/www/html/admin/theme/pages/info.tpl',
      1 => 1479572560,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/head.tpl' => 1,
    'file:parts/nav.tpl' => 1,
    'file:LICENSE' => 1,
    'file:parts/scripts.tpl' => 1,
  ),
),false)) {
function content_58307e5529c0d5_52546925 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:parts/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>
    <div id="wrapper">

      <?php $_smarty_tpl->_subTemplateRender("file:parts/nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Informacje</h1>
                    </div>
                    <div class="col-lg-6">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Informacje o systemie
                        </div>
                        <div class="panel-body">
                            <p><h4>Wersja:</h4> Alpha 0.0.1</p>
                            <p><h4>Wykonawca:</h4> <a href="http://merruem.eu">"Grupa Merruem" Patryk Kurzeja</a>
                            <p><h4>Autorzy:</h4>
                              <ul>
                                <li><a href="http://patrykkurzeja.pl">Patryk Kurzeja</a></li>
                                <li>Patryk Hansz</li>
                              </ul>
                            </p>
                            <h4>Informacje o bierzącej wersji:</h4>
                            <p>W huj do zrobiena ale jest spoko</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Informacje o motywie
                        </div>
                        <div class="panel-body">
                            <p><h4>Nazwa:</h4> Domyślny szablon - Bootstrap</p>
                            <p><h4>Wykonawca:</h4> <a href="http://merruem.eu">"Grupa Merruem" Patryk Kurzeja</a>
                            <p><h4>Autorzy:</h4>
                              <ul>
                                <li><a href="http://patrykkurzeja.pl">Patryk Kurzeja</a></li>
                                <li>Patryk Hansz</li>
                              </ul>
                            </p>
                            <h4>Informacje dotyczące praw autorskich:</h4>
                            <p>Wszystkie prawa zastrzeżone. &copy Grupa Merruem 2016</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Licencja SB-Admin-2
                        </div>
                        <div class="panel-body">
                            <p><?php $_smarty_tpl->_subTemplateRender("file:LICENSE", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</p>
                        </div>
                      </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $_smarty_tpl->_subTemplateRender("file:parts/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</body>

</html>
<?php }
}
