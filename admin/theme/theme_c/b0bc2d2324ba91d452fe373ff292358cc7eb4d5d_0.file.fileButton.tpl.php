<?php
/* Smarty version 3.1.30, created on 2016-11-19 14:36:58
  from "/var/www/html/admin/theme/parts/fileButton.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5830557a323a09_13254784',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0bc2d2324ba91d452fe373ff292358cc7eb4d5d' => 
    array (
      0 => '/var/www/html/admin/theme/parts/fileButton.tpl',
      1 => 1479440644,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5830557a323a09_13254784 (Smarty_Internal_Template $_smarty_tpl) {
?>
<button id="addFileButton" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addFile">
  <p class="fa fa-upload"></p> Dodaj plik
</button>
<?php }
}
