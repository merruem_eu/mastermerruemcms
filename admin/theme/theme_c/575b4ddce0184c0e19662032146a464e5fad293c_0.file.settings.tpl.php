<?php
/* Smarty version 3.1.30, created on 2016-11-19 19:11:19
  from "/var/www/html/admin/theme/pages/settings.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_583095c71ead04_69217699',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '575b4ddce0184c0e19662032146a464e5fad293c' => 
    array (
      0 => '/var/www/html/admin/theme/pages/settings.tpl',
      1 => 1479579014,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/head.tpl' => 1,
    'file:parts/nav.tpl' => 1,
    'file:parts/scripts.tpl' => 1,
  ),
),false)) {
function content_583095c71ead04_69217699 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:parts/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>
    <div id="wrapper">

      <?php $_smarty_tpl->_subTemplateRender("file:parts/nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Ustawienia</h1>
                    </div>
                    <div class="col-lg-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="col-lg-4">Zarzadzaj ustawieniami</div>
                            <form action="/admin/settings/add" method="POST">
                              <div class="col-lg-3">
                                <input class="form-control" placeholder="Nazwa" name="name">
                              </div>
                              <div class="col-lg-3">
                                <input class="form-control" placeholder="Wartość" name="value">
                              </div>
                              <button class="btn btn-success  btn-circle" type="submit"><i class="fa fa-plus"></i></button>
                            </form>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Nazwa</th>
                                        <th>Wartosc</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['settingsList']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
                                    <tr class="odd gradeX">

                                        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
</td>
                                        <td>
                                          <div class="col-lg-12">
                                            <form action="/admin/settings/edit" method="POST">
                                              <div class="col-lg-10">
                                                <input type="hidden" name="name" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
" />
                                                <input class="form-control" placeholder="<?php echo $_smarty_tpl->tpl_vars['v']->value->value;?>
" name="value">
                                              </div>
                                              <div class="col-lg-1">
                                                <button class="btn btn-primary btn-circle" type="submit">
                                                  <i class="fa fa-edit"></i>
                                                </button>
                                              </div>
                                            </form>
                                            <form action="/admin/settings/delete" method="POST">
                                              <div class="col-lg-1">
                                                <input type="hidden" name="name" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
" />
                                                <button class="btn btn-danger btn-circle" type="submit">
                                                  <i class="fa fa-trash-o"></i>
                                                </button>
                                              </div>
                                            </form>
                                          </div>
                                        </td>

                                    </tr>
                                  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                </tbody>
                              </table>
                            <!-- /.table-responsive -->
                            </div>
                        <!-- /.panel-body -->
                        </div>
                    <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $_smarty_tpl->_subTemplateRender("file:parts/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</body>

</html>
<?php }
}
