<?php
/* Smarty version 3.1.30, created on 2016-11-20 04:25:51
  from "/var/www/html/admin/theme/parts/nav/task.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_583117bfd5fd78_46174083',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3c40a868b7564e725b3d599cc9019ffdab1c8468' => 
    array (
      0 => '/var/www/html/admin/theme/parts/nav/task.tpl',
      1 => 1479612347,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_583117bfd5fd78_46174083 (Smarty_Internal_Template $_smarty_tpl) {
?>
<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-tasks sysstat">
        <li>
            <a href="#">
                <div>
                    <p>
                        <strong>Zużycie dysku: </strong>
                        <span class="pull-right text-muted"><?php echo $_smarty_tpl->tpl_vars['occupiedDiskSpace']->value;?>
GB / <?php echo $_smarty_tpl->tpl_vars['diskSpace']->value;?>
GB</span>
                    </p>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $_smarty_tpl->tpl_vars['diskUsage']->value;?>
" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $_smarty_tpl->tpl_vars['diskUsage']->value;?>
%">
                            <span class="sr-only"><?php echo $_smarty_tpl->tpl_vars['diskUsage']->value;?>
%</span>
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div>
                    <p>
                        <strong>Zużycie RAM: </strong>
                        <span class="pull-right text-muted"><i class="usedMem"></i>GB / <i class="totalMem"></i>GB</span>
                    </p>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-success barMem" role="progressbar" " aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $_smarty_tpl->tpl_vars['diskUsage']->value;?>
%">
                            <span class="sr-only percMem">0%</span>
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li class="divider"></li>
    </ul>
    <!-- /.dropdown-tasks -->
</li>
<?php }
}
