<?php
/* Smarty version 3.1.30, created on 2016-11-19 17:30:39
  from "/var/www/html/admin/theme/pages/statsoft.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58307e2f8e12c9_96667724',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '72f5ea444b95ace58a321954fadb52ed4a39340e' => 
    array (
      0 => '/var/www/html/admin/theme/pages/statsoft.tpl',
      1 => 1479573037,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/head.tpl' => 1,
    'file:parts/nav.tpl' => 1,
    'file:parts/scripts.tpl' => 1,
  ),
),false)) {
function content_58307e2f8e12c9_96667724 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:parts/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>
    <div id="wrapper">

      <?php $_smarty_tpl->_subTemplateRender("file:parts/nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Statystyki oprogramowania:</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-6">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Przeglądarki
                            <div class="pull-right">
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-browsers-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>


                  </div>

                  <div class="col-lg-6">

                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-bar-chart-o fa-fw"></i> Systemy operacyjne
                          <div class="pull-right">
                          </div>
                      </div>
                      <!-- /.panel-heading -->
                      <div class="panel-body">
                          <div id="morris-platforms-chart"></div>
                      </div>
                      <!-- /.panel-body -->
                  </div>


                </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $_smarty_tpl->_subTemplateRender("file:parts/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php echo '<script'; ?>
>$(function() {
        Morris.Donut({
            element: 'morris-browsers-chart',
            data: [
              <?php $_smarty_tpl->_assignInScope('i', 0);
?>
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['browserStat']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
              {
                label: "<?php echo $_smarty_tpl->tpl_vars['v']->value->count_id;?>
",
                value: <?php echo $_smarty_tpl->tpl_vars['v']->value->count;?>

            }<?php if ($_smarty_tpl->tpl_vars['i']->value != $_smarty_tpl->tpl_vars['browserStatCount']->value-1) {?>,
    <?php }?>
    <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);
?>
  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


          ],
            resize: true
        });
    });
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>$(function() {
    Morris.Donut({
        element: 'morris-platforms-chart',
        data: [
          <?php $_smarty_tpl->_assignInScope('i', 0);
?>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['osStat']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
          {
            label: "<?php echo $_smarty_tpl->tpl_vars['v']->value->count_id;?>
",
            value: <?php echo $_smarty_tpl->tpl_vars['v']->value->count;?>

        }<?php if ($_smarty_tpl->tpl_vars['i']->value != $_smarty_tpl->tpl_vars['osStatCount']->value-1) {?>,
<?php }
$_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


      ],
        resize: true
    });
});
<?php echo '</script'; ?>
>

</body>

</html>
<?php }
}
