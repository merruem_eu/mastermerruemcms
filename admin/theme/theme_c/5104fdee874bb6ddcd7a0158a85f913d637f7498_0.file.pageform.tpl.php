<?php
/* Smarty version 3.1.30, created on 2016-11-21 02:27:58
  from "/var/www/html/admin/theme/pages/pageform.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58324d9ed33551_11954511',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5104fdee874bb6ddcd7a0158a85f913d637f7498' => 
    array (
      0 => '/var/www/html/admin/theme/pages/pageform.tpl',
      1 => 1479681929,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/head.tpl' => 1,
    'file:parts/nav.tpl' => 1,
    'file:parts/scripts.tpl' => 1,
  ),
),false)) {
function content_58324d9ed33551_11954511 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:parts/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>
  <?php echo '<script'; ?>
 src="//cdn.tinymce.com/4/tinymce.min.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
>tinymce.init({ selector:'textarea' });<?php echo '</script'; ?>
>
    <div id="wrapper">

      <?php $_smarty_tpl->_subTemplateRender("file:parts/nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<form action="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
" method="POST">
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><?php if (isset($_smarty_tpl->tpl_vars['page']->value->title)) {?>Edycja strony<?php } else { ?>Nowa strona<?php }?></h1>
                        <!-- /.col-lg-12 -->
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-8">
                      <form role="form">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                              Panel edycji
                          </div>
                          <div class="panel-body">
                              <div class="form-group">
                                <label>Tytuł</label>
                                <?php if (isset($_smarty_tpl->tpl_vars['page']->value->content)) {?>
                                <input type="hidden" name="uid" value="<?php echo $_smarty_tpl->tpl_vars['page']->value->uid;?>
">
                                <input type="hidden" name="pid" value="<?php echo $_smarty_tpl->tpl_vars['page']->value->pid;?>
">
                                <?php }?>
                                <input name="title" class="form-control" value="<?php if (isset($_smarty_tpl->tpl_vars['page']->value->title)) {?> <?php echo $_smarty_tpl->tpl_vars['page']->value->title;?>
 <?php }?>">
                              </div>
                              <div class="form-group">
                                <label>Treść</label></br>
                                <textarea name="content" class="form-control" id="editor" ><?php if (isset($_smarty_tpl->tpl_vars['page']->value->content)) {
echo $_smarty_tpl->tpl_vars['page']->value->content;
}?></textarea>
                          </div>
                        </div>
                        <div class="panel-footer">
                          <button type="submit" class="btn btn-primary">Zapisz</button>
                        </div>
                      </div>
                        <!-- /.col-lg-12 -->
                      </div>

                      <div class="col-lg-4">
                        <div class="panel panel-primary">
                          <div class="panel-heading">
                            Primary Panel
                          </div>
                          <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                          </div>
                          <div class="panel-footer">
                            Panel Footer
                          </div>
                        </div>
                        <!-- /.col-lg-8 -->
                      </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
</form>
    </div>
    <!-- /#wrapper -->
<?php $_smarty_tpl->_subTemplateRender("file:parts/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</body>

</html>
<?php }
}
