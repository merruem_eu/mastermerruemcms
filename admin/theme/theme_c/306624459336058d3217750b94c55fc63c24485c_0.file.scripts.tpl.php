<?php
/* Smarty version 3.1.30, created on 2016-11-20 22:15:20
  from "/var/www/html/admin/theme/parts/scripts.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5832126816fcd7_19709125',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '306624459336058d3217750b94c55fc63c24485c' => 
    array (
      0 => '/var/www/html/admin/theme/parts/scripts.tpl',
      1 => 1479676517,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5832126816fcd7_19709125 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- jQuery -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/vendor/jquery/jquery.min.js"><?php echo '</script'; ?>
>

<!-- Bootstrap Core JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/vendor/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>

<!-- DataTables JavaScript -->
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/vendor/datatables/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/vendor/datatables-plugins/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/vendor/datatables-responsive/dataTables.responsive.js"><?php echo '</script'; ?>
>


<!-- Metis Menu Plugin JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/vendor/metisMenu/metisMenu.min.js"><?php echo '</script'; ?>
>

<!-- Morris Charts JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/vendor/raphael/raphael.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/vendor/morrisjs/morris.min.js"><?php echo '</script'; ?>
>

<!-- Custom Theme JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/dist/js/sb-admin-2.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/vendor/jquery-collagePlus/jquery.collagePlus.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/upload.js"><?php echo '</script'; ?>
>
<scipt src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/selectImage.js"><?php echo '</script'; ?>
>

  <!-- The template to display files available for upload -->
  <?php echo '<script'; ?>
 id="template-upload" type="text/x-tmpl">
  {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-upload fade">
          <td>
              <span class="preview"></span>
          </td>
          <td>
              <p class="name">{%=file.name%}</p>
              <strong class="error text-danger"></strong>
          </td>
          <td>
              <p class="size">Processing...</p>
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
          </td>
          <td>
              {% if (!i && !o.options.autoUpload) { %}
                  <button class="btn btn-primary start" disabled>
                      <i class="glyphicon glyphicon-upload"></i>
                      <span>Start</span>
                  </button>
              {% } %}
              {% if (!i) { %}
                  <button class="btn btn-warning cancel">
                      <i class="glyphicon glyphicon-ban-circle"></i>
                      <span>Cancel</span>
                  </button>
              {% } %}
          </td>
      </tr>
  {% } %}
  <?php echo '</script'; ?>
>
  <!-- The template to display files available for download -->
  <?php echo '<script'; ?>
 id="template-download" type="text/x-tmpl">
  {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-download fade">
          <td>
              <span class="preview">
                  {% if (file.thumbnailUrl) { %}
                      <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                  {% } %}
              </span>
          </td>
          <td>
              <p class="name">
                  {% if (file.url) { %}
                      <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                  {% } else { %}
                      <span>{%=file.name%}</span>
                  {% } %}
              </p>
              {% if (file.error) { %}
                  <div><span class="label label-danger">Error</span> {%=file.error%}</div>
              {% } %}
          </td>
          <td>
              <span class="size">{%=o.formatFileSize(file.size)%}</span>
          </td>
          <td>
              {% if (file.deleteUrl) { %}
                  <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                      <i class="glyphicon glyphicon-trash"></i>
                      <span>Delete</span>
                  </button>
                  <input type="checkbox" name="delete" value="1" class="toggle">
              {% } else { %}
                  <button class="btn btn-warning cancel">
                      <i class="glyphicon glyphicon-ban-circle"></i>
                      <span>Cancel</span>
                  </button>
              {% } %}
          </td>
      </tr>
  {% } %}
  <?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin/js/vendor/jquery.ui.widget.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"><?php echo '</script'; ?>
>
  <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
  <?php echo '<script'; ?>
 src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"><?php echo '</script'; ?>
>
  <!-- The Canvas to Blob plugin is included for image resizing functionality -->
  <?php echo '<script'; ?>
 src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"><?php echo '</script'; ?>
>
  <!-- blueimp Gallery script -->
  <?php echo '<script'; ?>
 src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"><?php echo '</script'; ?>
>
  <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin/js/jquery.iframe-transport.js"><?php echo '</script'; ?>
>
  <!-- The basic File Upload plugin -->
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin/js/jquery.fileupload.js"><?php echo '</script'; ?>
>
  <!-- The File Upload processing plugin -->
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin/js/jquery.fileupload-process.js"><?php echo '</script'; ?>
>
  <!-- The File Upload image preview & resize plugin -->
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin/js/jquery.fileupload-image.js"><?php echo '</script'; ?>
>
  <!-- The File Upload audio preview plugin -->
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin/js/jquery.fileupload-audio.js"><?php echo '</script'; ?>
>
  <!-- The File Upload video preview plugin -->
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin/js/jquery.fileupload-video.js"><?php echo '</script'; ?>
>
  <!-- The File Upload validation plugin -->
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin/js/jquery.fileupload-validate.js"><?php echo '</script'; ?>
>
  <!-- The File Upload user interface plugin -->
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin/js/jquery.fileupload-ui.js"><?php echo '</script'; ?>
>
  <!-- The main application script -->
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
/admin/js/main.js"><?php echo '</script'; ?>
>
  <!-- Custom Theme JavaScript -->
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/dist/js/sb-admin-2.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
/js/upload.js"><?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="/admin/theme/js/sysstat.js"><?php echo '</script'; ?>
>
<?php }
}
