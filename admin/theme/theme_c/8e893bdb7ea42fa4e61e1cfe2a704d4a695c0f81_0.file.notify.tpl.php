<?php
/* Smarty version 3.1.30, created on 2016-11-19 14:36:58
  from "/var/www/html/admin/theme/parts/nav/notify.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5830557a3207b7_50586980',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8e893bdb7ea42fa4e61e1cfe2a704d4a695c0f81' => 
    array (
      0 => '/var/www/html/admin/theme/parts/nav/notify.tpl',
      1 => 1478435271,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5830557a3207b7_50586980 (Smarty_Internal_Template $_smarty_tpl) {
?>
<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-alerts">
        <li>
            <a href="#">
                <div>
                    <i class="fa fa-comment fa-fw"></i> Powiadomienie
                    <span class="pull-right text-muted small">4 minutt temu</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>

    </ul>
    <!-- /.dropdown-alerts -->
<?php }
}
