<?php
/* Smarty version 3.1.30, created on 2016-11-20 23:15:56
  from "/var/www/html/admin/theme/pages/pages.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5832209c02d9c7_84243727',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1ecbafdb17440eedd3e334584694f05639638918' => 
    array (
      0 => '/var/www/html/admin/theme/pages/pages.tpl',
      1 => 1479680153,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/head.tpl' => 1,
    'file:parts/nav.tpl' => 1,
    'file:parts/scripts.tpl' => 1,
  ),
),false)) {
function content_5832209c02d9c7_84243727 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:parts/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>

    <div id="wrapper">

      <?php $_smarty_tpl->_subTemplateRender("file:parts/nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Podstrony</h1>
                    </div>
                    <div class="col-lg-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Zarządzaj podstronami
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tytuł</th>
                                        <th>Autor</th>
                                        <th>Status</th>
                                        <th>Działania</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pagesList']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->pid;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->title;?>
</td>
                                        <td><a href="/admin/user/<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
 <?php echo $_smarty_tpl->tpl_vars['v']->value->lastname;?>
</a></td>
                                        <td class="center"><?php if ($_smarty_tpl->tpl_vars['v']->value->active) {?>Opublikowana<?php } else { ?>Niewidoczna<?php }?></td>
                                        <td class="center">
                                          <div class="col-lg-12">
                                              <form action="/admin/pages/edit" method="POST">
                                                  <input type="hidden" name="pid" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->pid;?>
">
                                                  <button type="submit" class="btn btn-primary btn-circle">
                                                      <i class="fa fa-edit"></i>
                                                  </button>
                                                </form>

                                                <form action="/admin/pages/toogle" method="POST">
                                                  <input type="hidden" name="pid" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->pid;?>
">
                                                  <button type="submit" class="btn btn-default btn-circle">
                                                    <i class="fa fa-eye-slash"></i>
                                                  </button>
                                                </form>

                                                <form action="/admin/pages/del" method="POST">
                                                  <input type="hidden" name="pid" value="<?php echo $_smarty_tpl->tpl_vars['v']->value->pid;?>
">
                                                  <button type="submit" class="btn btn-danger btn-circle">
                                                    <i class="fa fa-trash-o"></i>
                                                  </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


                                </tbody>
                              </table>
                            <!-- /.table-responsive -->
                            </div>
                        <!-- /.panel-body -->
                        </div>
                    <!-- /.panel -->
                    </div>
                  </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $_smarty_tpl->_subTemplateRender("file:parts/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php echo '<script'; ?>
>
    $(document).ready(function(){
        $('#dataTables').DataTable({
            responsive: true
        });
    });
    <?php echo '</script'; ?>
>
</body>

</html>
<?php }
}
