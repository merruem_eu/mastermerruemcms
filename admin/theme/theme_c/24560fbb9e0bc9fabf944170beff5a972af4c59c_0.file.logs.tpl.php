<?php
/* Smarty version 3.1.30, created on 2016-11-19 17:47:16
  from "/var/www/html/admin/theme/pages/logs.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_583082148cc173_00642707',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24560fbb9e0bc9fabf944170beff5a972af4c59c' => 
    array (
      0 => '/var/www/html/admin/theme/pages/logs.tpl',
      1 => 1479574034,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/head.tpl' => 1,
    'file:parts/nav.tpl' => 1,
    'file:parts/scripts.tpl' => 1,
  ),
),false)) {
function content_583082148cc173_00642707 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:parts/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>

    <div id="wrapper">

      <?php $_smarty_tpl->_subTemplateRender("file:parts/nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Logi</h1>
                    </div>
                    <div class="col-lg-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Przegladaj logi systemu
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Data</th>
                                        <th>Uzytkownik</th>
                                        <th>Adres IP</th>
                                        <th>Tresc</th>
                                    </tr>
                                </thead>
                                <tbody>

                                  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['logsList']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->logid;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->tdate;?>
</td>
                                        <td><a href="/admin/user/<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->user->name;?>
 <?php echo $_smarty_tpl->tpl_vars['v']->value->user->lastname;?>
</a></td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->u_ip;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->content;?>
</td>
                                    </tr>
                                  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                </tbody>
                              </table>
                            <!-- /.table-responsive -->
                            </div>
                        <!-- /.panel-body -->
                        </div>
                    <!-- /.panel -->
                    </div>
                  </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $_smarty_tpl->_subTemplateRender("file:parts/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php echo '<script'; ?>
>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            responsive: true
        });
    });
    <?php echo '</script'; ?>
>
</body>

</html>
<?php }
}
