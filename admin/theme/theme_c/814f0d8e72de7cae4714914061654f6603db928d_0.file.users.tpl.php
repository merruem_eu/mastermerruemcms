<?php
/* Smarty version 3.1.30, created on 2016-11-19 19:50:55
  from "/var/www/html/admin/theme/pages/users.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58309f0fb74959_02377705',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '814f0d8e72de7cae4714914061654f6603db928d' => 
    array (
      0 => '/var/www/html/admin/theme/pages/users.tpl',
      1 => 1479581075,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:parts/head.tpl' => 1,
    'file:parts/nav.tpl' => 1,
    'file:parts/scripts.tpl' => 1,
  ),
),false)) {
function content_58309f0fb74959_02377705 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:parts/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<body>

    <div id="wrapper">

      <?php $_smarty_tpl->_subTemplateRender("file:parts/nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Administratorzy</h1>

                    </div>
                    <div class="col-lg-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Zarzadzaj użytkownikami panelu admina
                            <a href="/admin/user/new" class="btn btn-success btn-circle">
                              <i class="fa fa-plus"></i>
                            </a></br>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Imię i nazwisko</th>
                                        <th>Login</th>
                                        <th>Email</th>
                                        <th>Działania</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['usersList']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
</td>
                                        <td><a href="/admin/user/<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value->name;?>
 <?php echo $_smarty_tpl->tpl_vars['v']->value->lastname;?>
</a></td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->login;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['v']->value->email;?>
</td>
                                        <td class="center">
                                          <a href="/admin/user/<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
/edit" class="btn btn-primary btn-circle">
                                            <i class="fa fa-edit"></i>
                                          </a>
                                          <form action="/admin/user/<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
" method="POST">
                                            <input type="hidden" name="option" value="out" />
                                            <button class="btn btn-warning btn-circle" type="submit">
                                                <i class="fa fa-power-off"></i></i>
                                            </button>
                                          </form>
                                          <a href="/admin/user/<?php echo $_smarty_tpl->tpl_vars['v']->value->uid;?>
/del" class="btn btn-danger btn-circle">
                                            <i class="fa fa-trash-o"></i>
                                          </a>
                                        </td>
                                    </tr>
                                  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                </tbody>
                              </table>
                            <!-- /.table-responsive -->
                            </div>
                        <!-- /.panel-body -->
                        </div>
                    <!-- /.panel -->
                    </div>
                  </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $_smarty_tpl->_subTemplateRender("file:parts/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php echo '<script'; ?>
>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            responsive: true
        });
    });
    <?php echo '</script'; ?>
>
</body>

</html>
<?php }
}
