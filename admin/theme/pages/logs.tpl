<!--{include "parts/head.tpl"}-->
<body>

    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Logi</h1>
                    </div>
                    <div class="col-lg-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Przegladaj logi systemu
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Data</th>
                                        <th>Uzytkownik</th>
                                        <th>Adres IP</th>
                                        <th>Tresc</th>
                                    </tr>
                                </thead>
                                <tbody>

                                  <!--{foreach from=$logsList item=v}-->
                                    <tr class="odd gradeX">
                                        <td><!--{$v->logid}--></td>
                                        <td><!--{$v->tdate}--></td>
                                        <td><a href="/admin/user/<!--{$v->uid}-->"><!--{$v->user->name}--> <!--{$v->user->lastname}--></a></td>
                                        <td><!--{$v->u_ip}--></td>
                                        <td><!--{$v->content}--></td>
                                    </tr>
                                  <!--{/foreach}-->
                                </tbody>
                              </table>
                            <!-- /.table-responsive -->
                            </div>
                        <!-- /.panel-body -->
                        </div>
                    <!-- /.panel -->
                    </div>
                  </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<!--{include "parts/scripts.tpl"}-->
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            responsive: true
        });
    });
    </script>
</body>

</html>
