<!--{include "parts/head.tpl"}-->
<body>
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->
<form action="<!--{$action}-->" method="POST">
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><!--{if isset($news->title)}-->Edycja strony<!--{else}-->Nowa strona<!--{/if}--></h1>
                        <!-- /.col-lg-12 -->
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-8">
                      <form role="form">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                              Panel edycji
                          </div>
                          <div class="panel-body">
                              <div class="form-group">
                                <label>Tytuł</label>
                                <input type="hidden" name="nid" value="<!--{$news->nid}-->">
                                <input class="form-control" name="title" value="<!--{if isset($news->title)}--> <!--{$news->title}--> <!--{/if}-->">
                              </div>
                              <div class="form-group">
                                <label>Treść</label></br>
                                <textarea class="form-control" id="editor" name="content"><!--{if isset($news->content)}--><!--{$news->content}--><!--{/if}--></textarea>
                          </div>
                          <div class="form-group">
                            <label>Treść skrócona</label></br>
                            <textarea class="form-control" id="editor" name="short_content"><!--{if isset($news->short_content)}--><!--{$news->short_content}--><!--{/if}--></textarea>
                      </div>
                        </div>
                        <div class="panel-footer">
                          <button type="submit" class="btn btn-primary">Zapisz</button>
                        </div>
                      </div>
                        <!-- /.col-lg-12 -->
                      </div>

                      <div class="col-lg-4">
                        <div class="panel panel-primary">
                          <div class="panel-heading">
                            Primary Panel
                          </div>
                          <div class="panel-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                          </div>
                          <div class="panel-footer">
                            Panel Footer
                          </div>
                        </div>
                        <!-- /.col-lg-8 -->
                      </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
</form>
    </div>
    <!-- /#wrapper -->
<!--{include "parts/scripts.tpl"}-->

</body>

</html>
