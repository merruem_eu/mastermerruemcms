<!--{include "parts/head.tpl"}-->
<body>

    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Administratorzy</h1>

                    </div>
                    <div class="col-lg-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Zarzadzaj użytkownikami panelu admina
                            <a href="/admin/user/new" class="btn btn-success btn-circle">
                              <i class="fa fa-plus"></i>
                            </a></br>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Imię i nazwisko</th>
                                        <th>Login</th>
                                        <th>Email</th>
                                        <th>Działania</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <!--{foreach from=$usersList item=v}-->
                                    <tr class="odd gradeX">
                                        <td><!--{$v->uid}--></td>
                                        <td><a href="/admin/user/<!--{$v->uid}-->"><!--{$v->name}--> <!--{$v->lastname}--></a></td>
                                        <td><!--{$v->login}--></td>
                                        <td><!--{$v->email}--></td>
                                        <td class="center">
                                          <a href="/admin/user/<!--{$v->uid}-->/edit" class="btn btn-primary btn-circle">
                                            <i class="fa fa-edit"></i>
                                          </a>
                                          <form action="/admin/user/<!--{$v->uid}-->" method="POST">
                                            <input type="hidden" name="option" value="out" />
                                            <button class="btn btn-warning btn-circle" type="submit">
                                                <i class="fa fa-power-off"></i></i>
                                            </button>
                                          </form>
                                          <a href="/admin/user/<!--{$v->uid}-->/del" class="btn btn-danger btn-circle">
                                            <i class="fa fa-trash-o"></i>
                                          </a>
                                        </td>
                                    </tr>
                                  <!--{/foreach}-->
                                </tbody>
                              </table>
                            <!-- /.table-responsive -->
                            </div>
                        <!-- /.panel-body -->
                        </div>
                    <!-- /.panel -->
                    </div>
                  </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--{include "parts/scripts.tpl"}-->
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            responsive: true
        });
    });
    </script>
</body>

</html>
