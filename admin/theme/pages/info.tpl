<!--{include "parts/head.tpl"}-->
<body>
    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Informacje</h1>
                    </div>
                    <div class="col-lg-6">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Informacje o systemie
                        </div>
                        <div class="panel-body">
                            <p><h4>Wersja:</h4> Alpha 0.0.1</p>
                            <p><h4>Wykonawca:</h4> <a href="http://merruem.eu">"Grupa Merruem" Patryk Kurzeja</a>
                            <p><h4>Autorzy:</h4>
                              <ul>
                                <li><a href="http://patrykkurzeja.pl">Patryk Kurzeja</a></li>
                                <li>Patryk Hansz</li>
                              </ul>
                            </p>
                            <h4>Informacje o bierzącej wersji:</h4>
                            <p>W huj do zrobiena ale jest spoko</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Informacje o motywie
                        </div>
                        <div class="panel-body">
                            <p><h4>Nazwa:</h4> Domyślny szablon - Bootstrap</p>
                            <p><h4>Wykonawca:</h4> <a href="http://merruem.eu">"Grupa Merruem" Patryk Kurzeja</a>
                            <p><h4>Autorzy:</h4>
                              <ul>
                                <li><a href="http://patrykkurzeja.pl">Patryk Kurzeja</a></li>
                                <li>Patryk Hansz</li>
                              </ul>
                            </p>
                            <h4>Informacje dotyczące praw autorskich:</h4>
                            <p>Wszystkie prawa zastrzeżone. &copy Grupa Merruem 2016</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Licencja SB-Admin-2
                        </div>
                        <div class="panel-body">
                            <p><!--{include "LICENSE"}--></p>
                        </div>
                      </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--{include "parts/scripts.tpl"}-->

</body>

</html>
