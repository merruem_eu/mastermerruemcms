<!--{include "parts/head.tpl"}-->
<body>

    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Podstrony</h1>
                    </div>
                    <div class="col-lg-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Zarządzaj podstronami
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tytuł</th>
                                        <th>Autor</th>
                                        <th>Status</th>
                                        <th>Działania</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <!--{foreach from=$pagesList item=v}-->
                                    <tr class="odd gradeX">
                                        <td><!--{$v->pid}--></td>
                                        <td><!--{$v->title}--></td>
                                        <td><a href="/admin/user/<!--{$v->uid}-->"><!--{$v->name}--> <!--{$v->lastname}--></a></td>
                                        <td class="center"><!--{if $v->active}-->Opublikowana<!--{else}-->Niewidoczna<!--{/if}--></td>
                                        <td class="center">
                                          <div class="col-lg-12">
                                              <form action="/admin/pages/edit" method="POST">
                                                  <input type="hidden" name="pid" value="<!--{$v->pid}-->">
                                                  <button type="submit" class="btn btn-primary btn-circle">
                                                      <i class="fa fa-edit"></i>
                                                  </button>
                                                </form>

                                                <form action="/admin/pages/toogle" method="POST">
                                                  <input type="hidden" name="pid" value="<!--{$v->pid}-->">
                                                  <button type="submit" class="btn btn-default btn-circle">
                                                    <i class="fa fa-eye-slash"></i>
                                                  </button>
                                                </form>

                                                <form action="/admin/pages/del" method="POST">
                                                  <input type="hidden" name="pid" value="<!--{$v->pid}-->">
                                                  <button type="submit" class="btn btn-danger btn-circle">
                                                    <i class="fa fa-trash-o"></i>
                                                  </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <!--{/foreach}-->

                                </tbody>
                              </table>
                            <!-- /.table-responsive -->
                            </div>
                        <!-- /.panel-body -->
                        </div>
                    <!-- /.panel -->
                    </div>
                  </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--{include "parts/scripts.tpl"}-->
    <script>
    $(document).ready(function(){
        $('#dataTables').DataTable({
            responsive: true
        });
    });
    </script>
</body>

</html>
