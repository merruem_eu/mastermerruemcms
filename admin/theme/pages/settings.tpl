<!--{include "parts/head.tpl"}-->
<body>
    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Ustawienia</h1>
                    </div>
                    <div class="col-lg-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="col-lg-4">Zarzadzaj ustawieniami</div>
                            <form action="/admin/settings/add" method="POST">
                              <div class="col-lg-3">
                                <input class="form-control" placeholder="Nazwa" name="name">
                              </div>
                              <div class="col-lg-3">
                                <input class="form-control" placeholder="Wartość" name="value">
                              </div>
                              <button class="btn btn-success  btn-circle" type="submit"><i class="fa fa-plus"></i></button>
                            </form>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Nazwa</th>
                                        <th>Wartosc</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <!--{foreach from=$settingsList item=v}-->
                                    <tr class="odd gradeX">

                                        <td><!--{$v->name}--></td>
                                        <td>
                                          <div class="col-lg-12">
                                            <form action="/admin/settings/edit" method="POST">
                                              <div class="col-lg-10">
                                                <input type="hidden" name="name" value="<!--{$v->name}-->" />
                                                <input class="form-control" placeholder="<!--{$v->value}-->" name="value">
                                              </div>
                                              <div class="col-lg-1">
                                                <button class="btn btn-primary btn-circle" type="submit">
                                                  <i class="fa fa-edit"></i>
                                                </button>
                                              </div>
                                            </form>
                                            <form action="/admin/settings/delete" method="POST">
                                              <div class="col-lg-1">
                                                <input type="hidden" name="name" value="<!--{$v->name}-->" />
                                                <button class="btn btn-danger btn-circle" type="submit">
                                                  <i class="fa fa-trash-o"></i>
                                                </button>
                                              </div>
                                            </form>
                                          </div>
                                        </td>

                                    </tr>
                                  <!--{/foreach}-->
                                </tbody>
                              </table>
                            <!-- /.table-responsive -->
                            </div>
                        <!-- /.panel-body -->
                        </div>
                    <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--{include "parts/scripts.tpl"}-->

</body>

</html>
