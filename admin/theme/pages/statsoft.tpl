<!--{include "parts/head.tpl"}-->
<body>
    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Statystyki oprogramowania:</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-6">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Przeglądarki
                            <div class="pull-right">
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-browsers-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>


                  </div>

                  <div class="col-lg-6">

                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-bar-chart-o fa-fw"></i> Systemy operacyjne
                          <div class="pull-right">
                          </div>
                      </div>
                      <!-- /.panel-heading -->
                      <div class="panel-body">
                          <div id="morris-platforms-chart"></div>
                      </div>
                      <!-- /.panel-body -->
                  </div>


                </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--{include "parts/scripts.tpl"}-->
    <script>$(function() {
        Morris.Donut({
            element: 'morris-browsers-chart',
            data: [
              <!--{$i = 0}-->
              <!--{foreach from=$browserStat item=v}-->
              {
                label: "<!--{$v->count_id}-->",
                value: <!--{$v->count}-->
            }<!--{if $i != $browserStatCount - 1}-->,
    <!--{/if}-->
    <!--{$i = $i + 1}-->
  <!--{/foreach}-->

          ],
            resize: true
        });
    });
</script>
<script>$(function() {
    Morris.Donut({
        element: 'morris-platforms-chart',
        data: [
          <!--{$i = 0}-->
          <!--{foreach from=$osStat item=v}-->
          {
            label: "<!--{$v->count_id}-->",
            value: <!--{$v->count}-->
        }<!--{if $i != $osStatCount - 1}-->,
<!--{/if}-->
<!--{$i = $i + 1}-->
<!--{/foreach}-->

      ],
        resize: true
    });
});
</script>

</body>

</html>
