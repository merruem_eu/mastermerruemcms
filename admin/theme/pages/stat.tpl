<!--{include "parts/head.tpl"}-->
<body>

    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Statystyki</h1>
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Odwiedziny
                                    <div class="pull-right">

                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div id="morris-area-chart"></div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                          </div>
                            <div class="col-lg-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Strony
                                    <div class="pull-right">
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div id="morris-pages-chart"></div>
                                </div>
                                <!-- /.panel-body -->
                            </div>


                          </div>

                          <div class="col-lg-6">

                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  <i class="fa fa-bar-chart-o fa-fw"></i> Proporcja wizyt do odwiedzajacych
                                  <div class="pull-right">
                                  </div>
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body">
                                  <div id="morris-visits-chart"></div>
                              </div>
                              <!-- /.panel-body -->
                          </div>


                        </div>

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--{include "parts/scripts.tpl"}-->
    <script>$(function() {

        Morris.Area({
            element: 'morris-area-chart',
            data: [
<!--{$i = 0}-->
<!--{foreach from=$timeStat item=v}-->
          { Czas: '<!--{$v->count_id}-->', odwiedziny: <!--{$v->count}--> }<!--{if $i != $timeStatCount - 1}-->,
  <!--{/if}-->
  <!--{$i = $i + 1}-->
<!--{/foreach}-->
            ],
            xkey: 'Czas',
            ykeys: ['odwiedziny'],
            labels: ['Wyświetlenia'],
            pointSize: 2,
            hideHover: 'auto',
            resize: true
        });

        Morris.Donut({
            element: 'morris-pages-chart',
            data: [
              <!--{$i = 0}-->
              <!--{foreach from=$pageStat item=v}-->
              {
                label: "<!--{$v->count_id}-->",
                value: <!--{$v->count}-->
            }<!--{if $i != $pageStatCount - 1}-->,
    <!--{/if}-->
    <!--{$i = $i + 1}-->
  <!--{/foreach}-->

          ],
            resize: true
        });


        Morris.Donut({
            element: 'morris-visits-chart',
            data: [
              {
                label: "Wizyty",
                value: <!--{$sesions}-->
            },{
              label: "Załadownia",
              value: <!--{$reloads}-->
            }
          ],
            resize: true
        });
    });
</script>

</body>

</html>
