<!--{include "parts/head.tpl"}-->
<body>
    <!--{include "parts/modals/selectImage.tpl"}-->
    <!--{include "parts/modals/addFile.tpl"}-->



    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Informacje o koncie: <!--{$user->name}--> <!--{$user->lastname}--></h1>
                    </div>
                    <div class="col-lg-5">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Informacje o <!--{$user->name}--> <!--{$user->lastname}-->
                        </div>
                        <div class="panel-body">
                          <div class="form-group">
                            <label>Imię i nazwisko:</label>
                            <p class="form-control-static"><!--{$user->name}--> <!--{$user->lastname}--></p>
                          </div>
                          <div class="form-group">
                            <label>Login:</label>
                            <p class="form-control-static"><!--{$user->login}--></p>
                          </div>
                          <div class="form-group">
                            <label>Adres email:</label>
                            <p class="form-control-static"><!--{$user->email}--></p>
                          </div>
                        </div>
                        <div class="panel-footer">
                            <a class="btn btn-primary">Edytuj informacje</a>
                            <a class="btn btn-primary">Zmień hasło</a>
                            <form action="/admin/user/<!--{$user->uid}-->" method="POST">
                              <input type="hidden" name="option" value="out" />
                              <button class="btn btn-danger" type="submit">
                                <i class="fa  fa-power-off"></i> Wyloguj z wszystkich urządzeń</button>
                            </form>
                        </div>
                    </div>
                    </div>

                    <div class="col-lg-5">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Avatar
                        </div>
                        <div class="panel-body">
                          <img src="https://www.fanspole.com/assets/default_user-c283cfbc3d432e22b1d2f1eef515d0b9.png" alt="..." class="img-rounded">
                        </div>
                        <div class="panel-footer">
                          <button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#selectImage">
                            <p class="fa fa-plus"></p> Wybierz plik
                          </button>
                          <button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#addFile">
                            <p class="fa fa-upload"></p> Dodaj plik
                          </button>
                        </div>
                    </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--{include "parts/scripts.tpl"}-->

</body>

</html>
