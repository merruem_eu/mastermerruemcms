<!--{include "parts/head.tpl"}-->
<body>

    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Wiadomosci</h1>
                    </div>
                    <div class="col-lg-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Skrzynka odbiorcza
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Tytuł</th>
                                        <th>Autor</th>
                                        <th>Działania</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <!--{foreach from=$messagesList item=v}-->
                                    <tr class="odd gradeX <!--{if $v->opened == 0}--> <!--{if $v->alert}--> danger <!--{else}--> active <!--{/if}--><!--{/if}-->">
                                        <td><!--{$v->tdate}--></td>
                                        <td><!--{$v->title}--></td>
                                        <td><!--{$v->autor}--> (<!--{$v->mail}-->)</td>
                                        <td class="center">
                                          <div class="col-lg-12">

                                            </div>
                                        </td>
                                    </tr>
                                    <!--{/foreach}-->

                                </tbody>
                              </table>
                            <!-- /.table-responsive -->
                            </div>
                        <!-- /.panel-body -->
                        </div>
                    <!-- /.panel -->
                    </div>
                  </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--{include "parts/scripts.tpl"}-->
    <script>
    $(document).ready(function(){
        $('#dataTables').DataTable({
            responsive: true
        });
    });
    </script>
</body>

</html>
