<!--{include "parts/head.tpl"}-->
<body>

    <div id="wrapper">

      <!--{include "parts/nav.tpl"}-->

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Newsy</h1>
                    </div>
                    <div class="col-lg-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Zarządzaj newsami
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tytuł</th>
                                        <th>Autor</th>
                                        <th>Status</th>
                                        <th>Data utworzenia:</th>
                                        <th>Działania</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
                                        <td>1</td>
                                        <td>Przykładowa strona 1</td>
                                        <td>Patryk Kurzeja</td>
                                        <td class="center">Opublikowany</td>
                                        <td> 01-01-2016</td>
                                        <td class="center">
                                          <a type="button" class="btn btn-primary btn-circle">
                                            <i class="fa fa-edit"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr class="even gradeC">
                                        <td>2</td>
                                        <td>Przykładowa strona 2</td>
                                        <td>Patryk Kurzeja</td>
                                        <td class="center">Niewidoczny</td>
                                        <td> 01-01-2016</td>
                                        <td class="center">C</td>
                                    </tr>
                                </tbody>
                              </table>
                            <!-- /.table-responsive -->
                            </div>
                        <!-- /.panel-body -->
                        </div>
                    <!-- /.panel -->
                    </div>
                  </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--{include "parts/scripts.tpl"}-->
    <script>
    $(document).ready(function(){
        $('#dataTables').DataTable({
            responsive: true
        });
    });
    </script>
</body>

</html>
