<div id="selectImage" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Wybierz obraz</h4>
        </div>
        <div class="modal-body">
          <div class="Collage">
            <!--{foreach from=$imagesList item=img}-->
              <img id="<!--{$img->fid}-->" src="/uploads/files/<!--{$img->location}--><!--{$img->name}-->"/>
            <!--{/foreach}-->
          </div>
        </div>
        <div class="modal-footer">
          <button id="exitBt" type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
          <!--{include "parts/fileButton.tpl"}-->
          <button id="sendBt" type="button" class="btn btn-primary">Wybierz</button>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
