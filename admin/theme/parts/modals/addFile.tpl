<div id="addFile" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="fileupload" action="http://<!--{$domain}-->/uploads/index.php" method="POST" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Dodanie pliku na serwer</h4>
        </div>
        <div class="modal-body">
          <!-- Redirect browsers with JavaScript disabled to the origin page -->
          <noscript><input type="hidden" name="redirect" value="<!--{$url}-->/admin"></noscript>
          <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
          <span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Dodaj plik</span>
            <input type="file" name="files[]" multiple>
          </span>
          <div class="row fileupload-buttonbar">
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
              <!-- The global progress bar -->
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                  <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
              </div>
            </div>
            <table role="presentation" class="table table-striped"><tbody class="files"></tbody>
            </table>
          </div>
          <div class="modal-footer">
            <div class="col-lg-12">
              <!-- The fileinput-button span is used to style the file input field as button -->
              <button type="submit" class="btn btn-primary start">
                  <i class="glyphicon glyphicon-upload"></i>
                  <span>Rozpocznij wgrywanie</span>
              </button>
              <button type="reset" class="btn btn-warning cancel">
                  <i class="glyphicon glyphicon-ban-circle"></i>
                  <span>Anuluj wgrywanie</span>
              </button>
              <button type="button" class="btn btn-danger delete">
                  <i class="glyphicon glyphicon-trash"></i>
                  <span>Usuń</span>
              </button>
              <input type="checkbox" class="toggle">
              <!-- The global file processing state -->
              <span class="fileupload-process"></span>
              <button id="exitBt" type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
          </div>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
