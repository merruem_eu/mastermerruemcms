<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-messages">
        <li>
            <a href="#">
                <div>
                    <strong>System</strong>
                    <span class="pull-right text-muted">
                        <em>Dzisiaj</em>
                    </span>
                </div>
                <div>Nieudane logowanie.</div>
            </a>
        </li>
        <li class="divider"></li>
    </ul>
    <!-- /.dropdown-messages -->
</li>
