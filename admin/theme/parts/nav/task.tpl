<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-tasks sysstat">
        <li>
            <a href="#">
                <div>
                    <p>
                        <strong>Zużycie dysku: </strong>
                        <span class="pull-right text-muted"><!--{$occupiedDiskSpace}-->GB / <!--{$diskSpace}-->GB</span>
                    </p>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<!--{$diskUsage}-->" aria-valuemin="0" aria-valuemax="100" style="width: <!--{$diskUsage}-->%">
                            <span class="sr-only"><!--{$diskUsage}-->%</span>
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div>
                    <p>
                        <strong>Zużycie RAM: </strong>
                        <span class="pull-right text-muted"><i class="usedMem"></i>GB / <i class="totalMem"></i>GB</span>
                    </p>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-success barMem" role="progressbar" " aria-valuemin="0" aria-valuemax="100" style="width: <!--{$diskUsage}-->%">
                            <span class="sr-only percMem">0%</span>
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li class="divider"></li>
    </ul>
    <!-- /.dropdown-tasks -->
</li>
