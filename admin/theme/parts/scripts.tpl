<!-- jQuery -->
<script src="<!--{$theme_url}-->/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<!--{$theme_url}-->/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- DataTables JavaScript -->
    <script src="<!--{$theme_url}-->/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<!--{$theme_url}-->/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<!--{$theme_url}-->/vendor/datatables-responsive/dataTables.responsive.js"></script>


<!-- Metis Menu Plugin JavaScript -->
<script src="<!--{$theme_url}-->/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="<!--{$theme_url}-->/vendor/raphael/raphael.min.js"></script>
<script src="<!--{$theme_url}-->/vendor/morrisjs/morris.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<!--{$theme_url}-->/dist/js/sb-admin-2.js"></script>

<script src="<!--{$theme_url}-->/vendor/jquery-collagePlus/jquery.collagePlus.min.js"></script>
<script src="<!--{$theme_url}-->/js/upload.js"></script>
<scipt src="<!--{$theme_url}-->/js/selectImage.js"></script>

  <!-- The template to display files available for upload -->
  <script id="template-upload" type="text/x-tmpl">
  {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-upload fade">
          <td>
              <span class="preview"></span>
          </td>
          <td>
              <p class="name">{%=file.name%}</p>
              <strong class="error text-danger"></strong>
          </td>
          <td>
              <p class="size">Processing...</p>
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
          </td>
          <td>
              {% if (!i && !o.options.autoUpload) { %}
                  <button class="btn btn-primary start" disabled>
                      <i class="glyphicon glyphicon-upload"></i>
                      <span>Start</span>
                  </button>
              {% } %}
              {% if (!i) { %}
                  <button class="btn btn-warning cancel">
                      <i class="glyphicon glyphicon-ban-circle"></i>
                      <span>Cancel</span>
                  </button>
              {% } %}
          </td>
      </tr>
  {% } %}
  </script>
  <!-- The template to display files available for download -->
  <script id="template-download" type="text/x-tmpl">
  {% for (var i=0, file; file=o.files[i]; i++) { %}
      <tr class="template-download fade">
          <td>
              <span class="preview">
                  {% if (file.thumbnailUrl) { %}
                      <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                  {% } %}
              </span>
          </td>
          <td>
              <p class="name">
                  {% if (file.url) { %}
                      <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                  {% } else { %}
                      <span>{%=file.name%}</span>
                  {% } %}
              </p>
              {% if (file.error) { %}
                  <div><span class="label label-danger">Error</span> {%=file.error%}</div>
              {% } %}
          </td>
          <td>
              <span class="size">{%=o.formatFileSize(file.size)%}</span>
          </td>
          <td>
              {% if (file.deleteUrl) { %}
                  <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                      <i class="glyphicon glyphicon-trash"></i>
                      <span>Delete</span>
                  </button>
                  <input type="checkbox" name="delete" value="1" class="toggle">
              {% } else { %}
                  <button class="btn btn-warning cancel">
                      <i class="glyphicon glyphicon-ban-circle"></i>
                      <span>Cancel</span>
                  </button>
              {% } %}
          </td>
      </tr>
  {% } %}
  </script>
  <script src="<!--{$url}-->/admin/js/vendor/jquery.ui.widget.js"></script>
  <script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
  <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
  <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
  <!-- The Canvas to Blob plugin is included for image resizing functionality -->
  <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
  <!-- blueimp Gallery script -->
  <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
  <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
  <script src="<!--{$url}-->/admin/js/jquery.iframe-transport.js"></script>
  <!-- The basic File Upload plugin -->
  <script src="<!--{$url}-->/admin/js/jquery.fileupload.js"></script>
  <!-- The File Upload processing plugin -->
  <script src="<!--{$url}-->/admin/js/jquery.fileupload-process.js"></script>
  <!-- The File Upload image preview & resize plugin -->
  <script src="<!--{$url}-->/admin/js/jquery.fileupload-image.js"></script>
  <!-- The File Upload audio preview plugin -->
  <script src="<!--{$url}-->/admin/js/jquery.fileupload-audio.js"></script>
  <!-- The File Upload video preview plugin -->
  <script src="<!--{$url}-->/admin/js/jquery.fileupload-video.js"></script>
  <!-- The File Upload validation plugin -->
  <script src="<!--{$url}-->/admin/js/jquery.fileupload-validate.js"></script>
  <!-- The File Upload user interface plugin -->
  <script src="<!--{$url}-->/admin/js/jquery.fileupload-ui.js"></script>
  <!-- The main application script -->
  <script src="<!--{$url}-->/admin/js/main.js"></script>
  <!-- Custom Theme JavaScript -->
  <script src="<!--{$theme_url}-->/dist/js/sb-admin-2.js"></script>
  <script src="<!--{$theme_url}-->/js/upload.js"></script>
  <script src="/admin/theme/js/sysstat.js"></script>
