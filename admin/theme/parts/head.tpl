<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nowy CMS</title>

    <!-- Bootstrap Core CSS -->
    <link href="<!--{$theme_url}-->/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="<!--{$theme_url}-->/vendor/jquery-collagePlus/css/transitions.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<!--{$theme_url}-->/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<!--{$theme_url}-->/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="<!--{$theme_url}-->/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
  <link href="<!--{$theme_url}-->/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<!--{$theme_url}-->/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<!--{$theme_url}-->/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="<!--{$theme_url}-->/css/jquery.fileupload.css">
    <link rel="stylesheet" href="<!--{$theme_url}-->/css/jquery.fileupload-ui.css">


    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="<!--{$theme_url}-->/css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="<!--{$theme_url}-->/css/jquery.fileupload-ui-noscript.css"></noscript>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
