<?php

$sqlTables = array(
        "SETTINGS" =>
        'create table settings
        (
            name      varchar(50) PRIMARY KEY NOT NULL,
            value     varchar(100) not null
        ) ; ',

        "LOGINS" =>
        'create table logins
        (
            ukey      varchar(50) not null,
            uid       int not null,
            utime     int not null
        ) ; ',

        "USERS" =>
        'create table users
        (
            uid       int PRIMARY KEY AUTO_INCREMENT NOT NULL,
            name      varchar(20) not null,
            lastname  varchar(20) not null,
            email     varchar(50) not null,
            login     varchar(20) not null,
            md5pass   varchar(100) not null,
            active    int default 1 not null,
            avatar    int
        ) ; ',

        "PAGES" =>
        'create table pages
        (
            pid       int PRIMARY KEY AUTO_INCREMENT NOT NULL,
            title     varchar(50) not null,
            content   varchar(50000) not null,
            uid       int default 1 not null,
            active    int default 1 not null
        ) ; ',

        "NEWS" =>
        'create table news
        (
            nid               int PRIMARY KEY AUTO_INCREMENT NOT NULL,
            title             varchar(50) not null,
            content           varchar(10000) not null,
  			    short_content     varchar(1000) not null,
            uid               int not null,
            tdate             date not null,
            active            int default 1 not null
         ) ; ',


         "MESSAGES" =>
         'create table messages
          (
            mid       int PRIMARY KEY AUTO_INCREMENT NOT NULL,
            title     varchar(50) not null,
            content   varchar(10000) not null,
            autor	    varchar(50) not null,
            mail      varchar(150) not null,
            opened    int default 0,
            tdate     date not null
          ) ; ',


          "STAT_COUNTS" =>
          'create table stat_counts
          (
            count_id  varchar(50) PRIMARY KEY NOT NULL,
            type      varchar(25) NOT NULL,
            count     int not null
          ) ; ',


          "COMMENTS" =>
          'create table comments
          (
            cid       int PRIMARY KEY AUTO_INCREMENT NOT NULL,
            content   varchar(10000) not null,
            autor	    varchar(50) not null,
            tdate     date not null,
      			accepted  int default 0 not null
          ) ; ',


          "LOGS" =>
          'create table logs
          (
            logid     int PRIMARY KEY AUTO_INCREMENT NOT NULL,
            uid       int,
            tdate     datetime not null,
            u_ip      varchar(16),
            content   text
          ) ; ',


          "MENUSITES" =>
          'create table menusites
          (
            msid		int PRIMARY KEY AUTO_INCREMENT NOT NULL,
            title   varchar(30) not null,
            link    varchar(100) not null
          ) ; ',


          "FILES" =>
          'create table files
          (
            fid               int PRIMARY KEY AUTO_INCREMENT NOT NULL,
            name              varchar(115) not null,
            location          varchar(255) not null,
            server_location   varchar(255) not null,
            tdatetime         datetime not null,
            mother_image      varchar(255),
            size              varchar(10)
          ) ;',

          "NEWSLETTER" =>
          'create table newsletter
          (
            lid       int PRIMARY KEY AUTO_INCREMENT NOT NULL,
            name      varchar(25) not null,
            mail      varchar(150) not null,
            code      varchar(150) not null,
            tdate     datetime not null,
            active    int default 1
          );'
);
?>
