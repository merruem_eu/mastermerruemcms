<?php
  $dirDB = dirname(dirname(dirname(__FILE__))) . "/system/class/db.class.php";
  $dirLogs = dirname(dirname(dirname(__FILE__))) . "/system/class/logs.class.php";
  include($dirLogs);
  include($dirDB);
  include("dbTheme/dbExamplesData.php");

  $db = new Database();

  foreach ($sqlExample as $key => $value) {
    $db->queryNoFetch($value);
  }

  try{
    $filename = "tmp/settings.tmp";
    if ( !file_exists($filename) ) {
      throw new Exception('File not found.');
    }
    $dane = fread(fopen($filename, "r"), filesize($filename));
    if ( !$dane ) {
      throw new Exception('File open failed.');
    }
  }catch(Exception $e){
      exit('Blad otwarcia pliku konfiguracyjnego: '.$e->getMessage());
  }
  $dane = unserialize($dane);

  foreach($dane as $key => $value){
    $sql = "INSERT INTO settings (name, value) VALUES ('$key', '$value')";
    $db->queryNoFetch($sql);
  }

  try{
    $filename = "tmp/user.tmp";
    if ( !file_exists($filename) ) {
      throw new Exception('File not found.');
    }
    $dane = fread(fopen($filename, "r"), filesize($filename));
    if ( !$dane ) {
      throw new Exception('File open failed.');
    }
  }catch(Exception $e){
      exit('Blad otwarcia pliku konfiguracyjnego: '.$e->getMessage());
  }
  $dane = unserialize($dane);

  $name = $dane['name'];
  $lastname = $dane['lastname'];
  $email = $dane['email'];
  $login = $dane['login'];
  $pass = $dane['md5pass'];

  $sql = "INSERT INTO users (name, lastname, email, login, md5pass)
          VALUES('$name', '$lastname', '$email', '$login', '$pass')";

  $db->queryNoFetch($sql);

  $preJSON = array('status' => 0);
  echo json_encode($preJSON);


?>
