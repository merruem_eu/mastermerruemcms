<?php
  $dirDB = dirname(dirname(dirname(__FILE__))) . "/system/class/db.class.php";
  $dirLogs = dirname(dirname(dirname(__FILE__))) . "/system/class/logs.class.php";
  include($dirLogs);
  include($dirDB);
  include("dbTheme/tables.php");

  $db = new Database();
  $dbName = $db->getName();
  $sql = "SELECT concat('DROP TABLE IF EXISTS `', table_name, '`;')
          FROM `information_schema`.`tables`
          WHERE `table_schema` = '$dbName';";
  $drop = $db->queryNoFetch($sql);
  while($row = $drop->fetch()){
    $sql = $row[0];
    $db->queryNoFetch($sql);
  }

  foreach ($sqlTables as $key => $value) {
    $db->queryNoFetch($value);
  }

  $preJSON = array('status' => 0);
  echo json_encode($preJSON);


?>
