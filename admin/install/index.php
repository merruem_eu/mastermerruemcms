<?php

  $step = 1;
  if(isset($_GET['p'])){
      $step = $_GET['p'];
  }

  function page2(){
    if(isset($_POST['key'])){

      if(!is_dir("tmp")) mkdir("tmp", 0777, True);

      $url = 'http://92.222.66.178/CMS/api/validekey.php';
      $data = array('key' => $_POST['key']);

      $options = array(
        'http' => array(
          'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
          'method'  => 'POST',
          'content' => http_build_query($data)
        )
      );
      $context  = stream_context_create($options);
      $result = file_get_contents($url, false, $context);
    //if ($result === FALSE) { /* Handle error */ }
      $result = json_decode($result);


      if($result->valide){
        $file = fopen("tmp/key.tmp", "w");
        $codeKey = $_POST['key'];
        fwrite($file, $codeKey);
        fclose($file);
        return true;
      }
    }
    return false;
  }

  function page3(){
    $sole = md5(uniqid(rand(), true));
    $data = array("PageTitle"            => $_POST['title'],
                  "PageName"             => $_POST['title'],
                  "domain"               => $_POST['domain'],
                  "sole"                 => $sole,
                  "QuantityNewsInHome"   => 10
            );
    $file = fopen("tmp/settings.tmp", "w");
    $codeData = serialize($data);
    fwrite($file, $codeData);
    fclose($file);
    $file = fopen("tmp/sole.tmp", "w");
    fwrite($file, $sole);
    fclose($file);
  }

  function page4(){
    try{
      $filename = "tmp/sole.tmp";
      if ( !file_exists($filename) ) {
        throw new Exception('File not found.');
      }
      $dane = fread(fopen($filename, "r"), filesize($filename));
      if ( !$dane ) {
        throw new Exception('File open failed.');
      }
    }catch(Exception $e){
        exit('Blad otwarcia pliku konfiguracyjnego: '.$e->getMessage());
    }
    $sole = $dane;
    $data = array("name"            => $_POST['name'],
                  "lastname"        => $_POST['lastname'],
                  "login"           => $_POST['username'],
                  "email"           => $_POST['mail'],
                  "md5pass"         => md5($sole . $_POST['password'])
            );
    $codeData = serialize($data);
    $file = fopen("tmp/user.tmp", "w");
    fwrite($file, $codeData);
    fclose($file);
  }

function page5(){
  try{
    $filename = "tmp/settings.tmp";
    if ( !file_exists($filename) ) {
      throw new Exception('File not found.');
    }
    $dane = fread(fopen($filename, "r"), filesize($filename));
    if ( !$dane ) {
      throw new Exception('File open failed.');
    }
  }catch(Exception $e){
      exit('Blad otwarcia pliku konfiguracyjnego: '.$e->getMessage());
  }
  $data = unserialize($dane);
  $db['host'] = $_POST['host'];
  $db['pass'] = $_POST['password'];
  $db['user'] = $_POST['user'];
  $db['name'] = $_POST['name'];
  $conf['database'] = $db;
  $conf['debugMode'] = 1;
  $conf['domain'] = $data['domain'];
  $conf['url'] = "http://" . $data['domain'];
  $confSerialized = serialize($conf);
  $data = $confSerialized;
  $file = fopen("tmp/db.tmp", "w");
  fwrite($file, $data);
  fclose($file);
}

 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="/admin/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <div id="cont">

        <div class="panel panel-default">
          <div class="row contWew">
            <!--CONTENT-->
              <?php
              switch($step){
                case 1:
                  include("pages/1.html");
                  break;
                case 2:
                  include("pages/2.html");
                  break;
                case 3:
                  $k = page2();
                  if($k)
                    include("pages/3.html");
                  else
                    include("pages/err.html");
                  break;
                case 4:
                  page3();
                  include("pages/4.html");
                  break;
                case 5:
                  page4();
                  include("pages/5.html");
                  break;
                case 6:
                  page5();
                  include("pages/6.html");
                  break;
                case 7:
                  include("pages/7.html");
                  break;
              }
              ?>

          <div class="col-md-12 center">
            <samp><center>&copy Patryk Kurzeja and Patryk Hansz 2016</center></samp>
          </div>
            <!--END_CONTENT-->
        </div>
      </div>
    </div>


    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/admin/theme/vendor/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
