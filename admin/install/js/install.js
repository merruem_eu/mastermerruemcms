$(document).ready(function(){
  $("#hide").hide();
  $("#hideSuccess").hide();
  $("#hideAlert").hide();
  $('#next').hide();
  for(var i = 1; i < 6; i++){
    $("#hideSuccess" + i).hide();
    $("#hideAlert" + i).hide();
  }

  $("#install").click(function(){
    $("#install").hide();
    dataString = "";
    $.ajax({
      type: 'POST',
      url: 'createConfig.php',
      data: dataString,

      success: function(data) {
        var data = jQuery.parseJSON( data );
        $("#hide").show();
        if( data.status == 0 ){
          $("#hideSuccess").show();
          $('#hideSuccess1').show();

          $.ajax({
            type: 'POST',
            url: 'addTables.php',
            data: dataString,

            success: function(data) {
              var data = jQuery.parseJSON( data );
              $("#hide").show();
              if( data.status == 0 ){
                $("#hideSuccess").show();
                $('#hideSuccess2').show();
                $('#hideSuccess3').show();

                $.ajax({
                  type: 'POST',
                  url: 'addData.php',
                  data: dataString,

                  success: function(data) {
                    var data = jQuery.parseJSON( data );
                    $("#hide").show();
                    if( data.status == 0 ){
                      $("#hideSuccess").show();
                      $('#hideSuccess4').show();

                      $.ajax({
                        type: 'POST',
                        url: 'rmTmp.php',
                        data: dataString,

                        success: function(data) {
                          var data = jQuery.parseJSON( data );
                          $("#hide").show();
                          if( data.status == 0 ){
                            $("#hideSuccess").show();
                            $('#hideSuccess5').show();

                            $('#next').show();
                          }
                          else{
                            $("#hideAlert").show();
                            $('#hideAlert5').show();
                          }
                        }
                      });


                    }
                    else{
                      $("#hideAlert").show();
                      $('#hideAlert4').show();
                    }
                  }
                });


              }
              else{
                $("#hideAlert").show();
                $('#hideAlert2').show();
                $('#hideAlert3').show();
              }
            }
          });


        }
        else{
          $("#hideAlert").show();
          $('#hideAlert1').show();
        }
      }
    });
  });
});
