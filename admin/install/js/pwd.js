function GeneratePassword() {
    if (parseInt(navigator.appVersion) <= 3) {
        alert("Sorry this only works in 4.0+ browsers");
        return true;
    }
    var length=8;
    var sPassword = "";
    var randomLength = 1;
    if (randomLength) {
        length = Math.random();
        length = parseInt(length * 100);
        length = (length % 7) + 6
    }
    for (i=0; i < length; i++) {
        numI = getRandomNum();
        sPassword = sPassword + String.fromCharCode(numI);
    }
    return sPassword;
}
function getRandomNum() {
    // between 0 - 1
    var rndNum = Math.random()
    // rndNum from 0 - 1000
    rndNum = parseInt(rndNum * 1000);
    // rndNum from 33 - 127
    rndNum = (rndNum % 94) + 33;
    return rndNum;
}
function checkPunc(num) {
    if ((num >=33) && (num <=47)) { return true; }
    if ((num >=58) && (num <=64)) { return true; }
    if ((num >=91) && (num <=96)) { return true; }
    if ((num >=123) && (num <=126)) { return true; }
    return false;
}

function scorePassword(pass) {
  var wynik = 0;
  var warianty = {
      cyfry: /\d/.test(pass),
      male: /[a-z]/.test(pass),
      duze: /[A-Z]/.test(pass),
      specjalne: /\W/.test(pass),
      dlugosc: pass.length > 7
  };
  for(var war in warianty)
    if(warianty[war] == true) wynik += 100 / 5;

  return parseInt(wynik);
}

$(document).ready(function(){
  $("#unvisible").hide();
  $("#password").on("keypress keyup keydown", function() {
        var wynik = scorePassword($(this).val());
        var textTab = ["Bardzo słabe", "Słabe", "Średnie", "Mocne", "Bardzo mocne", "Doskonałe!"];
        $("#pwdStrong").css("width", wynik + "%");
        $("#pwdStrong").html(textTab[wynik / 20]);
  });

  $("#generate").click(function(){
    $("#password").val(GeneratePassword());
    $("#password").attr("type", "text");
    $("#visible").hide();
    $("#unvisible").show();
    var wynik = scorePassword($("#password").val());
    var textTab = ["Bardzo słabe", "Słabe", "Średnie", "Mocne", "Bardzo mocne", "Doskonałe!"];
    $("#pwdStrong").css("width", wynik + "%");
    $("#pwdStrong").html(textTab[wynik / 20]);
  });

  $("#visible").click(function(){
    $("#password").attr("type", "text");
    $("#visible").hide();
    $("#unvisible").show();
  });

  $("#unvisible").click(function(){
    $("#password").attr("type", "password");
    $("#visible").show();
    $("#unvisible").hide();
  });
});
