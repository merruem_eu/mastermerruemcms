$(function() {
        // HIDE elements
        $('#keyAlert').hide();
        $('#keySuccess').hide();
        $('#next').hide();
        $("#valide").attr('disabled','disabled');

        // Valide forms
        $('#key').on('blur', function() {
		        var input = $(this);
		        var key_length = input.val().length;
            var litPatt = /^[0123456789abcdefABCDEF]{32}$/;
		        if(input.val().match(litPatt)){
              $('.form-group').addClass("has-success");
              $("#valide").removeAttr('disabled');
		        }
		        else{
              $('.form-group').addClass("has-error");
              $("#valide").attr('disabled','disabled');
		        }
          });


        // AJAX response
        $("#valide").click(function() {
          var key = $('#key').val();
          var dataString = 'key='+ key;

          $.ajax({
            type: 'POST',
            url: 'validekey.php',
            data: dataString,

            success: function(data) {
              var data = jQuery.parseJSON( data );
              if( data.valide == '0' )
                $('#keyAlert').show();
              else{
                $('#keySuccess').show();
                $('#valide').hide();
                $('#next').show();
              }
            }
          });
        });
});
