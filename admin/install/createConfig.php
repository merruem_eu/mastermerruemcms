<?php
$result = 0;


try{
  $filename = "tmp/db.tmp";
  if ( !file_exists($filename) ) {
    throw new Exception('File not found.');
  }
  $dane = fread(fopen($filename, "r"), filesize($filename));
  if ( !$dane ) {
    throw new Exception('File open failed.');
  }
}catch(Exception $e){
    exit('Blad otwarcia pliku konfiguracyjnego: '.$e->getMessage());
}

  $dataSerialized = $dane;
  //print_r($dataSerialized);
  $data = unserialize($dataSerialized);
  $dataSerialized = serialize($data);
  $file1 = dirname(dirname(__FILE__)) . "/config.conf";
  $file2 = dirname(dirname(dirname(__FILE__))) . "/config.conf";

try{
  $file = fopen($file1, "w");
  fwrite($file, $dataSerialized);
  fclose($file);

  $file = fopen($file2, "w");
  fwrite($file, $dataSerialized);
  fclose($file);

  $file = fopen("config.conf", "w");
  fwrite($file, $dataSerialized);
  fclose($file);
}catch(Exception $e){
  $result = 1;
}
  $preJSON = array('status' => $result);
  echo json_encode($preJSON);


?>
