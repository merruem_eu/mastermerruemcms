<?php
  $dir = dirname(dirname(__FILE__));
  $dir_admin = dirname(__FILE__);

  include_once($dir . "/autoload.php");

  include_once($dir_admin . "/class/admin.class.php");
  include_once($dir_admin . "/class/analyze.class.php");
  include_once($dir_admin . "/class/news.admin.class.php");
  include_once($dir_admin . "/class/pages.admin.class.php");
  include_once($dir_admin . "/class/comments.admin.class.php");
  include_once($dir_admin . "/class/menu.admin.class.php");
  include_once($dir_admin . "/class/logs.admin.class.php");
  include_once($dir_admin . "/class/settings.admin.class.php");
  include_once($dir_admin . "/class/views.admin.class.php");
  include_once($dir_admin . "/class/messages.admin.class.php");
  include_once($dir . "/system/class/files.class.php");
  include_once($dir_admin . "/class/controler.admin.class.php");

?>
