
<?php

/*********************************************************
* Autor:                  Patryk Kurzeja
* Data utworzenia pliku:  02.10.2016
* Opis pliku:             Kontroler panelu admina
* Prawa dostepu:          755
* Wykonawca:              "Merruem" Patryk Kurzeja
**********************************************************/

// Procedury startowe

  error_reporting(E_ALL); // poziom raportowania, http://pl.php.net/manual/pl/function.error-reporting.php
  ini_set('display_errors', 1);

  include_once("autoload.php");

  $smarty = new Smarty();
  $admin = new Admin();
  $Controler = new AdminControler();

  $theme = dirname(__FILE__).'/theme';
  $systemDir = dirname(dirname(__FILE__));

  $smarty->setTemplateDir($theme);
  $smarty->setCompileDir($theme . '/theme_c');
  $smarty->setCacheDir($theme . '/cache');
  $smarty->setConfigDir($theme . '/configs');

  $smarty->left_delimiter = '<!--{';
  $smarty->right_delimiter = '}-->';

  $domain = $Controler->Settings->get("domain");
  $url = "http://" . $domain;
  $url = $url . "/admin";
  $smarty->assign("domain", $domain);
  $smarty->assign('url', "http://" . $domain);
  $smarty->assign('theme_url', $url . '/theme');
  $smarty->assign("upload_max_filesize", ini_get("upload_max_filesize")."B");

  $multiplier = 1024*1024*1024;
  $freeSpace = @disk_free_space(dirname(__FILE__))/$multiplier;
  $diskSpace = @disk_total_space(dirname(__FILE__))/$multiplier;
  $occupiedSpace = $diskSpace - $freeSpace;
  if($diskSpace) 
	$diskUsage = $occupiedSpace / $diskSpace * 100;
  else
	$diskUsage = 100;

  $smarty->assign("diskSpace", round($diskSpace, 2));
  $smarty->assign("occupiedDiskSpace", round($occupiedSpace, 2));
  $smarty->assign("diskUsage", round($diskUsage, 2));


  $Files = new Files();
  $smarty->assign("imagesList", $Files->getImages());
// weryfikacja uzytkownika



  if(isset($_GET['p']))
  {
    if(isset($_GET['s1']) && $_GET['p'] == 'login')
    {
      if($_GET['s1'] == 'out'){
        $admin->logout($_COOKIE['session_key']);
        header("Location: ". $url);
      }
    }
    elseif($_GET['p'] == 'login' && isset($_POST['username'])){
      $ret = $admin->login($_POST['username'], $_POST['password'], 0);
      if($ret){
        $smarty->assign('loginError', $ret);
      }else header("Location: ". $url);
    }
    elseif($_GET['p'] == 'login' && isset($_COOKIE['session_key']))
      if($admin->verify($_COOKIE['session_key']))
        header("Location: ". $url);
  }

//Wyciaganie parametrow

$s1 = $s2 = $s3 = False;
if(isset($_GET['s1'])) $s1 = $_GET['s1'];
if(isset($_GET['s2'])) $s2 = $_GET['s2'];
if(isset($_GET['s3'])) $s3 = $_GET['s3'];

// instrukcje sterujace

  if(isset($_COOKIE['session_key'])){
    if($admin->verify($_COOKIE['session_key'])){
      //zalogowany
      $admin = new Admin();
      $user = $admin->getLoginUserData($_COOKIE['session_key']);
      $smarty->assign('loginUser', $user);

      if(isset($_GET['p'])){

        switch($_GET['p']){
          case 'pages': $page = AdminViews::pages($s1, $s2, $s3,$smarty); break;
          case  'stat': $page = AdminViews::statistics($s1, $s2, $s3,$smarty); break;
          case  'info': $page = AdminViews::info($s1, $s2, $s3,$smarty); break;
          case  'user': $page = AdminViews::user($s1, $s2, $s3,$smarty); break;
          case 'users': $page = AdminViews::users($s1, $s2, $s3,$smarty); break;
          case 'logs' : $page = AdminViews::logs($s1, $s2, $s3,$smarty); break;
          case 'settings': $page = AdminViews::settings($s1, $s2, $s3,$smarty); break;
          case 'news': $page = AdminViews::news($s1, $s2, $s3,$smarty); break;
          case 'messages': $page = AdminViews::messages($s1, $s2, $s3,$smarty); break;
          default:      $page = '404'; break;
        }
      }
      else{
        $page = AdminViews::home($s1, $s2, $s3,$smarty);
      }
    }else{
      $page = 'login';
    }
  }else{
    $page = 'login';
  }

  $smarty->display("pages/" . $page . '.tpl');


?>
