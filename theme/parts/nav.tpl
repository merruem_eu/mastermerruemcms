
    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          {foreach from=$menu item=foo}
          <a class="blog-nav-item" href="{$foo->link}">{$foo->title}</a>
          {/foreach}
        </nav>
      </div>
    </div>
