<footer>
  <p>&copy; 2016 Patryk Kurzeja & Patryk Hansz</p>
</footer>
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{$theme}js/vendor/jquery.min.js"><\/script>')</script>
<script src="{$theme}js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{$theme}js/ie10-viewport-bug-workaround.js"></script>
