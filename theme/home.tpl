<!DOCTYPE html>
<html lang="pl">
{include "parts/head.tpl"}

  <body>

{include "parts/nav.tpl"}

<div class="container">

  <div class="blog-header">
    <h1 class="blog-title">{$PageName}</h1>
    <p class="lead blog-description">System CMS</p>
  </div>

    <div class="row">

      <div class="col-sm-12 blog-main">
      {foreach from=$news item=v}

        <div class="blog-post">
          <h2 class="blog-post-title">{$v->title}</h2>
          <p class="blog-post-meta">{$v->tdate} Autor: {$v->name} {$v->lastname}</a></p>
          {$v->short_content}
          <a class="btn btn-primary" href="/news/{$v->nid}">Czytaj dalej</a>
        </div>
      {/foreach}
      {if $tabs > 1}
      <nav aria-label="Page navigation">
        <ul class="pagination">
          <li{if $tab == 1} class="disabled"{/if}>
            <a href="/home/{$tab - 1}" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
            </a>
          </li>
          {$max = 1}
          {for $i=1 to $tabs}
            <li{if $i == $tab} class="active"{/if}><a href="/home/{$i}">{$i}</a></li>
          {$max = $i}
          {/for}
          <li{if $tab == $max} class="disabled"{/if}>
            <a href="/home/{$tab + 1}" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
            </a>
          </li>
        </ul>
      </nav>
      {/if}
     </div><!-- /.blog-main -->

   </div>


 {include "parts/foot.tpl"}
